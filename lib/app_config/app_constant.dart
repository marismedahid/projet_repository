import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AppConstants {
  // Application Constants
  static const String appName = 'generic_library';
  static const String logo = 'assets/images/group.png';
  static const String profile = 'assets/images/oval.png';

  // Regular Expression for Phone Number Validation
  static RegExp phoneRegExp = RegExp(r'^[2-4]{1}?[0-9]{7}$');

  // Colors
  static const Color primaryColor = Color.fromARGB(255, 125, 195, 252);
  static const Color grey = Color(0XFF818181);
  static const Color grey2 = Color(0XFF818288);
  static const Color orng = Color(0XFFFF6600);
  static const Color black11 = Color(0XFF363842);
  static const Color black1 = Color(0xFF515150);
  static const Color orn = Color(0xFFFF6E40);
  static const Color grey1 = Color(0xFF666666);
  static const Color orngx = Color(0XFFFF470D);
  static const Color orngopa = Color(0XFFFF470D);
  static const Color gren = Color(0XFF05B171);
  static const Color black = Color(0xFF101010);
  static const Color grenopac = Color(0XFF28C270);
  static const Color bb = Color(0XFF535763);
  static const Color bbb = Color(0XFF535763);
  static const Color bbg = Color(0XFF6C757D);
  static const Color bold = Color(0XFF121212);
  static const Color customOrange = Color(0xFFFF6600);
  static const Color roza = Color(0xFFFFF2E9);

  // Font Families (Not used in this snippet, can be extended if needed)
  // static const String fontRegular = 'Regular';
  // static const String fontMedium = 'Medium';
  // static const String fontSemibold = 'Semibold';
  // static const String fontBold = 'Bold';
}

//Date & Time formatters
var standardDateFormatter = DateFormat(
  'dd-MM-yyyy',
  Get.locale?.languageCode == "fr" ? "fr_FR" : "ar_SA",
);
