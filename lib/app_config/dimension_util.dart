library config.globals;

import 'dart:math';
import 'package:flutter/material.dart';

/// Returns the full width of the screen.
double fullWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

/// Returns the full height of the screen.
double fullHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

/// Calculates the size based on the diagonal percentage of the screen.
/// Use this method for determining font size set icon size
double calculateSize(BuildContext context, double diagonalPercentage) {
  double diagonal =
      sqrt(pow(fullWidth(context), 2) + pow(fullHeight(context), 2));
  return diagonal * diagonalPercentage;
}

double horizontalSpacer(
  BuildContext context,
) {
  return fullWidth(context) * 0.05;
}

double verticalSpacer(
  BuildContext context,
) {
  return fullWidth(context) * 0.05;
}

double iconSize10(BuildContext context) {
  return MediaQuery.of(context).size.width * .023;
}

double fontSize10(BuildContext context) {
  return MediaQuery.of(context).size.width * .024;
}
