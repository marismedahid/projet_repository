import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class HttpGateway {
  final String baseUrl = "";
  String token = "";

  Future<void> getToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.containsKey('token')) {
      token = sharedPreferences.getString('token')!;
    }
  }

  Future<http.Response> post(String url, dynamic data) async {
    print(data);
    await getToken();
    log(token);
    http.Response response = await http.post(
      Uri.parse(url),
      body: data,
      headers: {
        "Authorization": "Bearer $token",
        "Content-Type": 'application/json',
      },
    );
    return response;
  }

  Future<http.Response> get(String url) async {
    await getToken();
    log(token);
    http.Response response = await http.get(
      Uri.parse(url),
      headers: {
        "Authorization": "Bearer $token",
        "Content-Type": 'application/json',
      },
    );
    return response;
  }

  Future<http.Response> put(String url, dynamic data) async {
    await getToken();
    http.Response response = await http.put(
      Uri.parse(url),
      body: data,
      headers: {
        "Authorization": "Bearer $token",
        "Content-Type": 'application/json',
      },
    );
    return response;
  }

  Future<http.Response> delete(String url) async {
    await getToken();
    http.Response response = await http.delete(
      Uri.parse(url),
      headers: {
        "Authorization": "Bearer $token",
        "Content-Type": 'application/json',
      },
    );
    return response;
  }
}
