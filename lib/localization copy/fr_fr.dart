const Map<String, String> frFr = {
  'phoneNumber': 'Numéro de téléphone',
  'password': 'Mot de passe',
  'emptyField': 'Champ vide !',
  'hintText': 'Entrez votre texte ici',
  'invalidPhoneNumber': 'Numéro de téléphone invalide !',
  'loginTitle': 'Écran de connexion',
  'loginSubtitle': 'Bienvenue à l\'écran de connexion',
  'forgotPassword': 'Mot de passe oublié ?',
  'login': 'Se connecter',
  'doNotHaveAccount': 'Vous n\'avez pas de compte ?',
  'signUpTitle': 'Écran d\'inscription',
  'signUpSubtitle': 'Bienvenue à l\'écran d\'inscription',
  'nni': 'NNI',
  'email': 'E-mail',
  'username': 'Nom d\'utilisateur',
  'confirmPassword': 'Confirmation de mot de passe',
  'passwordsDoNotMatch': 'Les mots de passe ne sont identitiques !',
  'signUp': 'S\'inscrire',
  'alreadyHaveAccount': 'Vous avez déjà un compte ?',
  'forgotPasswordTitle': 'Mot de passe oublié',
  'forgotPasswordSubtitle': 'Bienvenue à l\'écran de Mot de passe oublié',
  'continue': 'Continuer',
  'tryWithEmail': 'Voulez-vous essayer avec l\'e-mail ?',
  'tryWithPhoneNumber': 'Voulez-vous essayer avec le téléphone ?',
  'checkInboxForVerifyingEmail':
      'Vérifiez votre boîte de réception, vous recevrez un e-mail de vérification.',
  'checkInboxForConfirmationCode':
      'Vérifiez votre boîte de réception, vous recevrez un email contenant un code de confirmation pour réinitialiser votre mot de passe.',
  'resetPasswordTitle': 'Réinitialisation de mot de passe',
  'resetPasswordSubtitle':
      'Veuillez remplir les champs pour changer votre mot de passe',
  'validationCode': 'Code de validation',
  'firstName': 'Prénom',
  'lastName': 'Nom',
  'gender': 'Genre',
  'male': 'Homme',
  'female': 'Femme',
  'address': 'Adresse',
  'country': 'Pays',
  'city': 'Ville',
  'birthDate': 'Date de naissance',
  'completeProfileTitle': 'Complétisation du Profile',
  'completeProfileSubtitle': 'Veuillez compléter votre profil pour continuer',
  'confirm': 'Confirmer',
  'NoDataToDisplay': 'Aucune donnée à afficher',
  "sessionExpired":
      "Votre session a expiré. Veuillez vous reconnecter pour continuer",
  "disconnect": "Se déconnecter",
  "incorrectPassword": "Mot de passe incorrect !",
  "logOut": "Voulez-vous vraiment vous déconnecter ?",
  "deleteAccount": "Supprimer votre compte",
  "deleteAccountQuestion": "Voulez-vous vraiment supprimer votre compte ?",
  "deleteAccountReason":
      "Indiquez la raison pour laquelle vous souhaitez supprimer votre compte",
  "deleteAccountReasonDescription":
      "Décrivez la raison pour laquelle vous souhaitez supprimer votre compte",
  "deleteAccountConfirmation":
      "Entrez votre mot de passe pour confirmer la suppression du compte",
  "back": "Retour",
  "next": "Suivant",
  "yes": "Oui",
  "no": "Non",
  "noData": "Aucune donnée",
  "error": "Erreur",
  "Complete": "Terminé",
  "Home": "Accueil",
  "create_account": "Créer un compte",

  /** Register HTTP Status */
  'register409':
      'Nom d\'utilisateur ou mot de passe déjà utilisé par un autre utilisateur !',
  'register400': 'Veuillez vérifier les données envoyées',
  'register500': 'Une erreur s\'est produite !',

  /** Login HTTP Status */
  'login400': 'Veuillez vérifier les données envoyées',
  'login403': 'Nom d\'utilisateur ou mot de passe incorrect !',
  'login500': 'Une erreur s\'est produite !',
  'login423': 'Vous devriez vérifier votre email!',

  /** Reset password with email HTTP Status */
  'resetPasswordWithEmail404':
      'Aucun utilisateur avec cette adresse e-mail n\'a été trouvé !',

  /** Update password HTTP Status */
  'updatePassword410': 'Le code OTP a expiré !',
  'updatePassword403': 'Le code OTP est incorrect !',

  /** Update profile HTTP Status */
  'updateProfile400': 'Votre profil ne peut pas être modifié.',

  /** Update profile HTTP Status */
  'getProfile401': 'Veuillez vous reconnecter',

  /** Verify email HTTP Status */
  'verifyEmail410': 'Impossible de vérifier l\'e-mail. L\'URL a expiré !',

  /** Exception */
  'exception': 'Une erreur s\'est produite !',
  'logToYourAccount': "Connectez-vous à votre compte.",
  'connectPlease': "Connectez-vous à votre compte s'il vous plaît",
  'home': 'Accueil',
  'messages': 'Messages',
  'orders': 'Commandes',
  'profile': 'Profil',
  'online': 'En ligne',
  'settings': 'Paramètres',
  'deleting_acount': 'Suppression du compte',
  "personal_data": "Données personnelles",
  'confirme_modification': 'Confirmer la modification',
};
