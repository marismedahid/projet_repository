const Map<String, String> enUS = {
  'phoneNumber': 'Phone Number',
  'password': 'Password',
  'emptyField': 'Empty Field !',
  'hintText': 'Enter your text here',
  'invalidPhoneNumber': 'Invalid Phone Number!',
  'loginTitle': 'Login Screen',
  'loginSubtitle': 'Welcome to the login screen',
  'forgotPassword': 'Forgot Password ?',
  'login': 'Login',
  'doNotHaveAccount': "Don't have an account ?",
  'signUpTitle': 'Sign Up Screen',
  'signUpSubtitle': 'Welcome to the sign-up screen',
  'nni': 'NNI',
  'email': 'Email',
  'username': 'username',
  'confirmPassword': 'Confirm Password',
  'passwordsDoNotMatch': 'Passwords do not match !',
  'signUp': 'Sign Up',
  'alreadyHaveAccount': 'Already have an account ?',
  'forgotPasswordTitle': 'Forgot Password',
  'forgotPasswordSubtitle': 'Welcome to the Forgot Password screen',
  'continue': 'Continue',
  'tryWithEmail': 'Want to try with email?',
  'tryWithPhoneNumber': 'Want to try with phone number ?',
  'checkInboxForVerifyingEmail':
      'Check your email inbox, you will receive a verification email.',
  'checkInboxForConfirmationCode':
      'Check your inbox, you will receive an email containing a confirmation code to reset your password.',
  'resetPasswordTitle': 'Reset Password',
  'resetPasswordSubtitle': 'Please fill in the fields to change your password',
  'validationCode': 'Validation Code',
  'firstName': 'First Name',
  'lastName': 'Last Name',
  'gender': 'Gender',
  'male': 'Male',
  'female': 'Female',
  'address': 'Address',
  'country': 'Country',
  'city': 'City',
  'birthDate': 'Birth Date',
  'completeProfileTitle': 'Complete Profile',
  'completeProfileSubtitle': 'Please complete your profile to continue',
  'confirm': 'Confirm',
  'NoDataToDisplay': 'No data to display',
  "sessionExpired": "Your session has expired. Please log in again to continue",
  "disconnect": "Disconnect",
  "incorrectPassword": "Incorrect password !",
  "logOut": "Do you really want to log out ?",
  "deleteAccountQuestion": "Do you really want to delete your account ?",
  "deleteAccount": "Delete your account",
  "deleteAccountReason":
      "Specify the reason why you want to delete your account",
  "deleteAccountReasonDescription":
      "Describe the reason why you want to delete your account",
  "deleteAccountConfirmation":
      "Enter your password to confirm account deletion",
  "back": "Back",
  "next": "Next",
  "yes": "Yes",
  "no": "No",
  "noData": "No data",
  "error": "Error",
  "Historique": "historical",
  "Commandes\n terminées": "Completed orders",
  "Commandes\n annulées": "Cancelled orders",
  "Référence Delivery: ": "Delivery Reference :",
  "Status": "Status",
  "Complete": "Complete",
  "Référence Commande:": "Order Reference",
  "Paiement :": "Payment",
  "Mode de paiement :": "Payment method",
  "Cette commande contient:": "This order contains",
  "Commencer": "Begin",
  "Adresse": "Address",
  "Numéro de Commande:": "Order Numbe",
  "Temps restant": "Time remaining",
  "Temps estimé": "Estimated time",
  "Message": "Message",
  "Commande": "Order",
  "Veuillez entrer du texte": "Please enter some text ",
  "Mots de passe ne correspondent pas":"The passwords do not match",
  "Le formulaire n'est pas valide.":"The form is not valid",

  /** Register HTTP Status */
  'register409': 'Username ou password already in use by another user !',
  'register400': 'Please check the data sent',
  'register500': 'An error has occurred !',

  /** Login HTTP Status */
  'login400': 'Please check the data sent',
  'login403': 'Username or password incorrect !',
  'login500': 'An error has occurred !',
  'login423': 'You should be verify your email !',

  /** Reset password with email HTTP Status */
  'resetPasswordWithEmail404': 'A user with this email address was not found !',

  /** Update password HTTP Status */
  'updatePassword410': 'The OTP Code has expired !',
  'updatePassword403': 'The OTP code is incorrect !',

  /** Update profile HTTP Status */
  'updateProfile400':
      'An error has occurred ! Your profile cannot be modified.',

  /** Update profile HTTP Status */
  'getProfile401': 'Please login again',

  /** Verify email HTTP Status */
  'verifyEmail410': 'Cannot verify email. URL has expired !',

  /** Exception */
  'exception': 'An error has occurred !',
  'logToYourAccount': "Log in to your account",
  'connectPlease': "Log in to your account please",
  'home': 'Home',
  'messages': 'Messages',
  'orders': 'Orders',
  'profile': 'Profile',
  'online': 'Online',
  'settings': 'Settings',
  'deleting_acount': 'Delete account',
  "personal_data": "Personal data",
  'confirme_modification': 'Confirme modification'
};
