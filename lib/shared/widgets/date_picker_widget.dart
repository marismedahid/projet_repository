import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

// ignore: must_be_immutable
class DatePickerWidget extends StatefulWidget {
  final double width;
  final String hintText;
  final Color? borderColor;
  final TextStyle? hintTextStyle;
  final Function(BuildContext) selectDate;
  DateTime? selectedDate = DateTime.now();

  DatePickerWidget(
      {Key? key,
      required this.width,
      required this.hintText,
      this.hintTextStyle,
      this.borderColor,
      this.selectedDate,
      required this.selectDate})
      : super(key: key);

  @override
  _DatePickerWidgetState createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  late bool firstUse = true;

  @override
  Widget build(BuildContext context) {
    String formattedDate = standardDateFormatter.format(widget.selectedDate!);
    return Container(
      width: widget.width,
      decoration: BoxDecoration(
          border: Border.all(
              color: widget.borderColor ?? AppConstants.primaryColor),
          borderRadius: BorderRadius.all(Radius.circular(15))),
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: fullWidth(context) * 0.02,
            vertical: fullHeight(context) * 0.017),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(firstUse ? widget.hintText : formattedDate.toString(),
                style: widget.hintTextStyle ??
                    TextStyle(
                      color: Colors.grey[600],
                      fontSize: calculateSize(context, 0.014),
                    )),
            GestureDetector(
              child: Icon(
                Icons.calendar_today_outlined,
                color: HexColor("#62C6FF"),
                size: calculateSize(context, 0.02),
              ),
              onTap: () {
                widget.selectDate(context);
                firstUse = false;
              },
            ),
          ],
        ),
      ),
    );
  }
}
