import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomeButtonWidget extends StatelessWidget {
  final String title;
  final TextStyle? titleTextStyle;
  final Function()? onTap;
  final double? width;
  final double? heightPercentage;
  final Color? buttonColor;
  final Color? titleColor;
  final Color? borderColor;
  final Widget? prefixWidget;
  final Widget? suffixWidget;
  final double? fontSize;
  final double? radius;
  final MainAxisAlignment? mainAxisAlignment;
  CustomeButtonWidget({
    Key? key,
    required this.title,
    this.titleTextStyle,
    required this.onTap,
    this.width,
    this.heightPercentage,
    this.buttonColor,
    this.titleColor,
    this.borderColor,
    this.prefixWidget,
    this.suffixWidget,
    this.fontSize,
    this.mainAxisAlignment,
    this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius!),
          color: buttonColor ?? Colors.white,
          border: Border.all(
            color: borderColor ?? AppConstants.primaryColor,
            width: 1.0,
          ),
        ),
        height: heightPercentage != null
            ? _getHeight(context, heightPercentage!)
            : _getHeight(context, 0.07),
        width: width ?? fullWidth(context),
        child: Row(
          mainAxisAlignment: mainAxisAlignment ?? MainAxisAlignment.center,
          children: [
            if (prefixWidget != null)
              Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: fullWidth(context) * 0.03),
                  child: prefixWidget),
            Text(
              title.tr,
              style: titleTextStyle ??
                  TextStyle(
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                    color: titleColor ?? Colors.blue,
                    fontSize: fontSize ?? calculateSize(context, 0.02),
                  ),
            ),
            if (suffixWidget != null)
              Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: fullWidth(context) * 0.03),
                  child: suffixWidget)
          ],
        ),
      ),
    );
  }

  _getHeight(BuildContext context, double percentage) {
    var orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.landscape) {
      return fullWidth(context) * percentage;
    } else {
      return fullHeight(context) * percentage;
    }
  }
}
