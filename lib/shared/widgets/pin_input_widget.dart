import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pinput/pinput.dart';

class PinInputWidget extends StatelessWidget {
  final TextEditingController controller;
  final PinTheme? pinTheme;
  final PinTheme? focusedPinTheme;
  final PinTheme? submittedPinTheme;
  final PinputAutovalidateMode? pinputAutovalidateMode;
  final PinTheme? errorPinTheme;
  final TextStyle? errorTextStyle;
  final Widget? separator;
  final String? Function(String?)? validator;
  final int? length;
  final Widget? obscuringWidget;
  const PinInputWidget({
    Key? key,
    required this.controller,
    this.pinTheme,
    this.focusedPinTheme,
    this.submittedPinTheme,
    this.pinputAutovalidateMode,
    this.errorPinTheme,
    this.errorTextStyle,
    this.separator,
    this.length,
    this.obscuringWidget,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Pinput(
        controller: controller,
        obscureText: true,
        defaultPinTheme: PinTheme(
            height: fullHeight(context) * 0.07,
            width: fullWidth(context),
            decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(color: AppConstants.primaryColor),
                borderRadius: BorderRadius.all(Radius.circular(15)))),
        focusedPinTheme: focusedPinTheme,
        submittedPinTheme: submittedPinTheme,
        pinputAutovalidateMode:
            pinputAutovalidateMode ?? PinputAutovalidateMode.disabled,
        errorPinTheme: errorPinTheme,
        errorTextStyle: errorTextStyle ??
            TextStyle(
                fontSize: calculateSize(context, 0.013),
                color: Colors.red[800]),
        obscuringWidget: obscuringWidget ??
            Icon(Icons.circle,
                color: Colors.black, size: calculateSize(context, 0.02)),
        validator: validator ??
            (value) {
              if (value == null || value.isEmpty) {
                return 'emptyField'.tr;
              }
              return null;
            },
        showCursor: true,
        onCompleted: (pin) => print(pin),
        length: length ?? 4,
      ),
    );
  }
}
