import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';

class LoadingButtonWidget extends StatelessWidget {
  final String? imagePath;
  final double? width;
  final double? height;
  const LoadingButtonWidget({Key? key, this.imagePath, this.width, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      imagePath ?? 'assets/images/loading.gif',
      width: width ?? fullWidth(context) * 0.8,
      height: height ?? fullHeight(context) * 0.1,
    );
  }
}
