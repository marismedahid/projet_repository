import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/shared/controllers/home_screen_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class CustomBottomNavBarHome extends StatelessWidget {
  const CustomBottomNavBarHome({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeScreenControllerImp>(
      builder: (controller) => BottomNavigationBar(
        items: controller.bottomNavigationBarItems,
        currentIndex: controller.selectedIndex.value,
        selectedItemColor: AppConstants.orng,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        unselectedItemColor: AppConstants.grey,
        unselectedLabelStyle: TextStyle(color: AppConstants.grey),
        onTap: (index) =>
            controller.onItemTapped(index), // Call onItemTapped on tap
      ),
    );
  }
}
