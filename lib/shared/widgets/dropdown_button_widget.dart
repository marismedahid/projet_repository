import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';

class DropdownButtonWidget extends StatelessWidget {
  final Color? borderSideColor;
  final Color? fillColor;
  final dynamic value;
  final TextStyle? valueStyle;
  final String hintText;
  final TextStyle? hintStyle;
  final List<dynamic> items;
  final Function(Object?)? onChanged;

  const DropdownButtonWidget(
      {Key? key,
      this.borderSideColor,
      this.fillColor,
      this.value,
      this.valueStyle,
      required this.hintText,
      this.hintStyle,
      required this.items,
      required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: borderSideColor ?? AppConstants.primaryColor),
          borderRadius: BorderRadius.circular(12),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: borderSideColor ?? AppConstants.primaryColor),
          borderRadius: BorderRadius.circular(12),
        ),
        filled: true,
        contentPadding:
            EdgeInsets.symmetric(horizontal: fullWidth(context) * 0.03),
        hintText: hintText,
        hintStyle: TextStyle(
            fontSize: calculateSize(context, 0.013),
            fontWeight: FontWeight.w400),
        fillColor: fillColor ?? Colors.transparent,
      ),
      value: value,
      onChanged: onChanged,
      items: items.map((value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString(),
              style: TextStyle(
                fontSize: calculateSize(context, 0.015),
                fontWeight: FontWeight.w400,
              )),
        );
      }).toList(),
    );
  }
}
