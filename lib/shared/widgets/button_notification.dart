import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ButtonNotification extends StatelessWidget {
  const ButtonNotification({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        elevation: 4, // Adjust the elevation (shadow) as needed
        shape: RoundedRectangleBorder(
          // Optional: Defines the shape of the card
          borderRadius:
              BorderRadius.circular(16.0), // Adjust the border radius as needed
        ),
        color: Colors.white, // Optional: Set the background color of the card
        child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                  16.0), // Adjust the border radius as needed

              color: Colors.white, // Background color of the container
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.1), // Shadow color
                  spreadRadius: 5, // Spread radius
                  blurRadius: 7, // Blur radius
                  offset: Offset(0, 3), // Offset from the container
                ),
              ],
            ),
            padding: EdgeInsets.all(16.0),
            child: SvgPicture.asset('assets/images/iconnotification.svg')),
      ),
    );
  }
}
