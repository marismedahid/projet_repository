import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/widgets/custom_switcher_widgte.dart';
import 'package:flutter/material.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  AppBarWidget({super.key});

  @override
  Size get preferredSize => const Size.fromHeight(70);
  @override
  Widget build(BuildContext context) {
    return AppBar(
        leading: Image.asset(
          AppConstants.profile,
          width: 200,
          height: 50,
        ),
        actions: [
          Text("En ligne",
              style: TextStyle(
                  fontSize: 16,
                  color: AppConstants.gren,
                  fontWeight: FontWeight.bold)),
          SizedBox(
            width: fullWidth(context) * 0.05,
          ),
          Container(
            margin: EdgeInsets.only(right: fullWidth(context) * 0.06),
            child: CustomSwitcher(
              width: 53,
              controller: CustomSwitcherController(),
            ),
          )
        ]);
  }
}
