import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';

class ClickableTextWidget extends StatelessWidget {
  final void Function()? onTap;
  final String text;
  const ClickableTextWidget({Key? key, required this.onTap, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Text(
        text,
        style: TextStyle(
          fontSize: calculateSize(context, 0.014),
          color: Colors.blue,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
