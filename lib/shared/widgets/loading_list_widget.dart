import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingListWidget extends StatelessWidget {
  final int? itemCount;
  LoadingListWidget({super.key, this.itemCount});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: fullWidth(context),
      child: ListView.separated(
        controller: ScrollController(keepScrollOffset: false),
        shrinkWrap: true,
        itemCount: itemCount ?? 2,
        separatorBuilder: (context, index) => SizedBox(
          height: fullHeight(context) * 0.01,
        ),
        itemBuilder: (context, index) => _loadingItem(context),
      ),
    );
  }
}

dynamic _loadingItem(BuildContext context) {
  return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.white38,
      child: Container(
        width: fullWidth(context),
        height: fullHeight(context) * 0.07,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12), color: Colors.grey[300]),
      ));
}
