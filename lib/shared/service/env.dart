import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Env {
  static const String STATIC_URI_BA =
      'http://192.168.100.214:8084/smartmssa/order-management/';

  var token;
  _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    token = localStorage.getString('token');
  }

  authData(data, url) async {
    var fullUrl = STATIC_URI_BA + url;
    return await http
        .post(Uri.parse(fullUrl), body: jsonEncode(data), headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    });
  }

  postData(data, String url) async {
    var fullUrl = STATIC_URI_BA + url;

    await _getToken();
    print("__________body________________");
    print(jsonEncode(data));
    return http.post(Uri.parse(fullUrl),
        body: jsonEncode(data), headers: _setHeaders());
  }

  getData(url) async {
    var fullUrl = STATIC_URI_BA + url;
    print(fullUrl);
    await _getToken();
    print(token);
    return await http.get(Uri.parse(fullUrl), headers: _setHeaders());
  }

  getDataWithoutToken(url) async {
    var fullUrl = STATIC_URI_BA + url;
    return await http.get(Uri.parse(fullUrl), headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    });
  }

  putData(data, url) async {
    var fullUrl = STATIC_URI_BA + url;
    await _getToken();
    return await http.put(Uri.parse(fullUrl),
        body: jsonEncode(data), headers: _setHeaders());
  }

  deleteData(url) async {
    var fullUrl = STATIC_URI_BA + url;
    await _getToken();
    return await http.delete(Uri.parse(fullUrl), headers: _setHeaders());
  }

  _setHeaders() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };
}
