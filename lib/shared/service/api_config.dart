import 'package:delivery_user_app/shared/service/abstract_api_config.dart';
import 'package:http/http.dart' as http;

class ApiConfig implements AbstractApiConfig {
  String? url; // API endpoint URL
  dynamic headers; // Request headers (e.g., authorization token)
  dynamic body; // Request body (e.g., JSON data)
  http.Client client; // HTTP client used to send the request to the API

  static const backendUrl =
      "http://192.168.100.214:8081/smartmssa/security"; // Base URL for the backend API

  static const emailVerificationEnabled =
      true; // Set to true to enable email verification

  static const loggedOutAfterTokenExpiration =
      true; // Set to true to enable login out after token expiration

  static const timeout = Duration(seconds: 20); // Http request timeout

  ApiConfig({required this.client, this.url, this.headers, this.body});

  // Perform an HTTP POST request
  Future<http.Response> post() {
    return this
        .client
        .post(Uri.parse("${backendUrl}/${url}"), body: body, headers: headers)
        .timeout(timeout);
  }

  // Perform an HTTP GET request
  Future<http.Response> get() {
    return this
        .client
        .get(Uri.parse("${backendUrl}/${url}"), headers: headers)
        .timeout(timeout);
  }

  // Perform an HTTP PUT request
  Future<http.Response> put() {
    return this
        .client
        .put(Uri.parse("${backendUrl}/${url}"), body: body, headers: headers)
        .timeout(timeout);
  }

  // Perform an HTTP DELETE request
  Future<http.Response> delete() {
    return this
        .client
        .delete(Uri.parse("${backendUrl}/${url}"), headers: headers)
        .timeout(timeout);
  }

  // Perform an HTTP PATCH request
  Future<http.Response> patch() {
    return this
        .client
        .patch(Uri.parse("${backendUrl}/${url}"), body: body, headers: headers)
        .timeout(timeout);
  }
}
