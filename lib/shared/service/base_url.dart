class BaseUrls {
  static var production = false;
  static var verification_email = true;
  static var url =
      'http://192.168.100.214:8081/smartmssa/livraisonmanagement/api';
  static var urlAuth = 'http://192.168.100.214:8081/smartmssa/security/auth/';
  static var urlUser = 'http://192.168.100.214:8081/smartmssa/security/user/';
  static var urlApiSuscriber = 'http://192.168.100.214:8081/api/subscriber';
  static var subscriptionPLanUrl =
      'http://192.168.100.214:8083/smartmssa/subscription-management/api';
  static var apiUrl = 'http://192.168.100.214:8081/api';
  static var apiUserManagement = 'http://192.168.100.214:8082/api';
  static var apiCommandeManagement = 'http://192.168.100.214:8084/api';
  //static var apidetailsorders = 'http://192.168.100.214:8084/api';

  static var apiRestaurantManagement = 'http://192.168.100.214:8082/api';
  static var apiMenuManagement = 'http://192.168.100.214:8082/api';
  static var apiReferentielManagement = 'http://192.168.100.214:8082/api';
  static var mockonUrl = 'http://localhost:3000';
  static var authUrl = 'auth';
  static var appVersion = 'v0.0.0';
}
