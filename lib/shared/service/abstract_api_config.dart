import 'package:http/http.dart' as http;

abstract class AbstractApiConfig {
  String? url;
  dynamic headers;
  dynamic body;

  Future<http.Response> post();
  Future<http.Response> get();
  Future<http.Response> put();
  Future<http.Response> delete();
  Future<http.Response> patch();
}
