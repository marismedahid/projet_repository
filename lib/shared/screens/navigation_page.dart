import 'package:delivery_user_app/features/user_management/controllers/auth_controller.dart';
import 'package:delivery_user_app/shared/controllers/home_screen_controller.dart';
import 'package:delivery_user_app/shared/widgets/custom_bottom_navigationbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NavigationPage extends StatelessWidget {
  NavigationPage({Key? key}) : super(key: key);
  final AuthController authController = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    Get.put(HomeScreenControllerImp());
    return GetBuilder<HomeScreenControllerImp>(
        builder: (controller) => Scaffold(
              bottomNavigationBar: CustomBottomNavBarHome(),
              body: controller.listPage.elementAt(controller.currentpage),
            ));
  }
}
