import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/shared/service/api_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../features/user_management/app_routes.dart';
import '../../features/user_management/controllers/auth_controller.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthController _authController = Get.put(AuthController());
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () {
      if (ApiConfig.loggedOutAfterTokenExpiration) {
        _authController.logoutAfterTokenExpiration(true);
      } else {
        Get.offAllNamed(UserManagementRoutes.signUp);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppConstants.orng,
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: fullWidth(context) * 0.2),
          child: const Image(image: AssetImage(AppConstants.logo)),
        ),
      ),
    );
  }
}
