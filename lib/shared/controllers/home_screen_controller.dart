import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/features/order/screens/homepage.dart';
import 'package:delivery_user_app/features/order/screens/suivi_commande.dart';
import 'package:delivery_user_app/features/order/widgets/info_suivi_commande.dart';
import 'package:delivery_user_app/features/user_management/controllers/user_controller.dart';
import 'package:delivery_user_app/features/user_management/screens/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';

abstract class HomeScreenController extends GetxController {
  changePage(int currentpage);
}

class HomeScreenControllerImp extends HomeScreenController {
  int currentpage = 0;
  bool changecolor = false;

  final RxInt selectedIndex = 0.obs;

  void onItemTapped(int index) {
    selectedIndex.value = index;

    changePage(index); // Call changePage to update currentpage
  }

  List<Widget> listPage = [
    Homepage(),
    SuiviCommande(),
    InfoSuiviCommande(),
    ProfileTab(),
    //Text('no information')
  ];

  List<BottomNavigationBarItem> bottomNavigationBarItems = [
    BottomNavigationBarItem(
      icon: Image.asset(
          'assets/images/homesanscolor.png'), // Set initial unselected color
      activeIcon: Image.asset('assets/images/home.png'), // Set selected color
      label: "Home".tr,
    ),
    BottomNavigationBarItem(
      icon: Image.asset(
          'assets/images/chat1.png'), // Set initial unselected color
      activeIcon: Image.asset('assets/images/chatC.png'), // Set selected color
      label: "Suivi Commande".tr,
    ),
    BottomNavigationBarItem(
      icon: SvgPicture.asset(
          'assets/images/doc.svg'), // Set initial unselected color
      activeIcon: SvgPicture.asset('assets/images/doc.svg',
          colorFilter: ColorFilter.mode(
              AppConstants.orng, BlendMode.srcIn)), // Set selected color
      label: "Commande".tr,
    ),
    BottomNavigationBarItem(
      icon: SvgPicture.asset(
          'assets/images/profile.svg'), // Set initial unselected color
      activeIcon: GestureDetector(
        child: SvgPicture.asset('assets/images/profile.svg',
            colorFilter: ColorFilter.mode(AppConstants.orng, BlendMode.srcIn)),
      ), // Set selected color
      label: "Profile".tr,
    ),
  ];

  @override
  changePage(int i) {
    currentpage = i;
    update();
  }
}
