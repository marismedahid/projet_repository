import 'package:delivery_user_app/app_route.dart';
import 'package:delivery_user_app/features/user_management/controllers/user_controller.dart';
import 'package:delivery_user_app/localization%20copy/translation.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

void main() => runApp(
      MyApp(),
    );

class MyApp extends StatelessWidget {
  // UserController userController = Get.put(UserController());
  MyApp({super.key});
  @override
  Widget build(BuildContext context) {
  
    // userController.getProfile(context);
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        translations: LocaleString(),
        locale: const Locale('fr', 'FR'),
        fallbackLocale: const Locale('fr', 'FR'),
        // localizationsDelegates: [
        //   GlobalMaterialLocalizations.delegate,
        //   GlobalWidgetsLocalizations.delegate,
        // ],
        // home: YourScreen(),

        initialRoute: AppRoutes.splash,
        getPages: AppRoutes.routes);
  }
}
