import 'package:flutter/material.dart';

class AnimatedLoadingContainer extends StatefulWidget {
  const AnimatedLoadingContainer(
      {super.key,
      required this.height,
      required this.width,
      required this.hasBorderRadius,
      required this.borderRadius, required this.isCircular});

  @override
  _AnimatedLoadingContainerState createState() =>
      _AnimatedLoadingContainerState();

  final double height;
  final double width;
  final bool hasBorderRadius;
  final double borderRadius;
  final bool isCircular;
}

class _AnimatedLoadingContainerState extends State<AnimatedLoadingContainer>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    // Initialize animation controller
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2), // Adjust duration as needed
    );

    // Create animation
    _animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));

    // Start animation
    _controller.repeat();
  }

  @override
  void dispose() {
    // Dispose of the animation controller when not needed
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      
      width: widget.width,
      height: widget.height, // Adjust height as needed
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [Colors.grey.withOpacity(0.5), Colors.grey.withOpacity(0.2)],
        ),
         borderRadius: widget.hasBorderRadius? BorderRadius.circular(widget.borderRadius) : null,
         shape: widget.isCircular? BoxShape.circle : BoxShape.rectangle
      ),
      child: AnimatedBuilder(
        animation: _controller,
        builder: (context, child) {
          return Stack(
            children: [
              Positioned(
                left: MediaQuery.of(context).size.width * _animation.value - 20,
                child: Container(
                  width: 20,
                  height: 100,
                  color: Colors.grey.withOpacity(0.4),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class YourScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Your Screen')),
      body: const Center(
        child: AnimatedLoadingContainer(
          height: 50,
          isCircular: true,
          width: 50,
          hasBorderRadius: false,
          borderRadius: 8,
        ),
      ),
    );
  }
}
