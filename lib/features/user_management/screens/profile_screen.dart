import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/app_routes.dart';
import 'package:delivery_user_app/features/user_management/controllers/auth_controller.dart';
import 'package:delivery_user_app/features/user_management/controllers/custom_bottom_navigation_bar_widget.dart';
import 'package:delivery_user_app/features/user_management/controllers/user_controller.dart';
import 'package:delivery_user_app/features/user_management/widgets/custom_bottom_navigation_bar.dart';
import 'package:delivery_user_app/features/user_management/widgets/custom_switcher_widgte.dart';
import 'package:delivery_user_app/features/user_management/widgets/horizontal_divider_widget.dart';
import 'package:delivery_user_app/features/user_management/widgets/profile_list_item_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/svg.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({super.key});
  final CustomBottomNavigationBarController
      customBottomNavigationBarController =
      CustomBottomNavigationBarController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Obx(() => customBottomNavigationBarController
            .items[customBottomNavigationBarController.currentIndex.value]),
        bottomNavigationBar: CustomBottomNavigationBar(
          controller: customBottomNavigationBarController,
        ),
      ),
    );
  }
}

class ProfileTab extends StatelessWidget {
  final AuthController authController = Get.put(AuthController());
  final UserController userController = UserController();
  ProfileTab({super.key});

  void changeSwitch() {}
  @override
  Widget build(BuildContext context) {
    userController.getProfile(context);
    return Obx(
      () => Stack(
        children: [
          Positioned(
              left: fullWidth(context) * .2,
              bottom: fullHeight(context) * 0.65,
              child: Image.asset("assets/images/pattern.png")),
          Padding(
            padding: EdgeInsets.only(
                top: fullHeight(context) * .05,
                left: fullWidth(context) * .09,
                right: fullWidth(context) * .05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "profile".tr,
                      style: GoogleFonts.inter(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: AppConstants.black),
                    ),
                    Row(
                      children: [
                        Text(
                          "online".tr,
                          style: GoogleFonts.inter(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF05B171)),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        CustomSwitcher(
                          controller: CustomSwitcherController(),
                          width: 53,
                        ),
                        SizedBox(
                          width: 15,
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: calculateSize(context, 0.01),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    iconSize: calculateSize(context, .05),
                    onPressed: () {},
                    icon: Container(
                        padding: EdgeInsets.all(calculateSize(context, 0.015)),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Color(0xFFFAFDFF),
                            boxShadow: [
                              BoxShadow(
                                spreadRadius: 0,
                                blurRadius: 20,
                                offset: Offset(11, 28),
                                color: Color(0x144E5A33),
                              )
                            ]),
                        child:
                            SvgPicture.asset("assets/images/notification.svg")),
                  ),
                ),
                userController.isLoading.value
                    ? CircularProgressIndicator()
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: calculateSize(context, .10),
                            height: calculateSize(context, .10),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.red,
                                image: DecorationImage(
                                    image: AssetImage(
                                      'assets/images/profile.png',
                                    ),
                                    fit: BoxFit.cover)),
                          ),
                          SizedBox(
                            width: fullWidth(context) * 0.02,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                userController.currentUser.username == null
                                    ? 'ali'
                                    : userController.currentUser.username!,
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16,
                                    color: Colors.black),
                              ),
                              Text(
                                userController.currentUser.phoneNumber == null
                                    ? '4545'
                                    : userController.currentUser.phoneNumber!,
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    color: Color(0xFF878787)),
                              ),
                              Text(
                                userController.currentUser.email == null
                                    ? 'g@gmail.com'
                                    : userController.currentUser.email!,
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    color: Color(0xFF878787)),
                              ),
                            ],
                          )
                        ],
                      ),
                HorizontalDivider(),
                Text(
                  "profile".tr,
                  style: GoogleFonts.inter(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Color(0xFF878787)),
                ),
                SizedBox(
                  height: fullHeight(context) * 0.02,
                ),
                ProfileListItem(
                  withForward: true,
                  onPressed: () {
                    Get.toNamed(UserManagementRoutes.editProfile);
                  },
                  icon: Icon(
                    Icons.person,
                    size: 20,
                  ),
                  label: "personal_data".tr,
                ),
                SizedBox(
                  height: fullHeight(context) * 0.02,
                ),
                ProfileListItem(
                  withForward: true,
                  onPressed: () {
                    Get.toNamed(UserManagementRoutes.profileSettings);
                  },
                  icon: Icon(
                    Icons.settings,
                    size: 20,
                  ),
                  label: "settings".tr,
                ),
                SizedBox(
                  height: fullHeight(context) * 0.02,
                ),
                ProfileListItem(
                  onPressed: () {},
                  icon: Icon(
                    Icons.delete,
                    size: 20,
                  ),
                  label: "deleting_acount".tr,
                ),
                Spacer(),
                Container(
                  margin: EdgeInsets.only(bottom: 30),
                  height: fullHeight(context) * 0.07,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      border: Border.all(width: 1, color: Color(0xFFD6D6D6))),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.logout,
                          color: Color(0xFFF14141),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        InkWell(
                          onTap: () async {
                            await authController.logout();
                          },
                          child: Text(
                            'disconnect'.tr,
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w600,
                                fontSize: 14,
                                color: Color(0xFFF14141)),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
