import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/controllers/custom_fields_controllers.dart';
import 'package:delivery_user_app/features/user_management/widgets/custom_fields.dart';
import 'package:delivery_user_app/features/user_management/widgets/edit_profile_image.dart';
import 'package:delivery_user_app/features/user_management/widgets/large_button.dart';
import 'package:delivery_user_app/features/user_management/widgets/page_title_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class EditProfileScreen extends StatelessWidget {
  const EditProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Stack(
        children: [
          Positioned(
              left: fullWidth(context) * .35,
              bottom: fullHeight(context) * 0.7,
              child: Image.asset("assets/images/pattern.png")),
          Padding(
            padding: EdgeInsets.only(
              top: fullHeight(context) * .05,
              left: fullWidth(context) * .09,
              right: fullWidth(context) * .05,
              bottom: fullHeight(context) * .005,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PageTitle(
                    title: "personal_data".tr,
                  ),
                  SizedBox(
                    height: calculateSize(context, 0.03),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: EditProfileImageWidget(),
                  ),
                  SizedBox(
                    height: calculateSize(context, 0.02),
                  ),
                  CustomTextFormField(
                      initialValue: "Sidi",
                      textFieldLabel: "firstName".tr,
                      isPassword: false,
                      customController: CustomTextFormFieldController()),
                  SizedBox(
                    height: calculateSize(context, 0.02),
                  ),
                  CustomTextFormField(
                      initialValue: "Mohamed",
                      textFieldLabel: "lastName".tr,
                      isPassword: false,
                      customController: CustomTextFormFieldController()),
                  SizedBox(
                    height: calculateSize(context, 0.02),
                  ),
                  CustomDropdown(
                      textFieldLabel: "gender".tr,
                      customDropdownController: CustomDropdownController()),
                  SizedBox(
                    height: calculateSize(context, 0.02),
                  ),
                  CustomTextFormField(
                      initialValue: "+222 46 44 10 99",
                      textFieldLabel: "phoneNumber".tr,
                      isPassword: false,
                      customController: CustomTextFormFieldController()),
                  SizedBox(
                    height: calculateSize(context, 0.02),
                  ),
                  CustomTextFormField(
                      initialValue: "Sidi.Mohamed@smartmssa.com",
                      textFieldLabel: "email".tr,
                      isPassword: false,
                      customController: CustomTextFormFieldController()),
                  SizedBox(
                    height: calculateSize(context, 0.02),
                  ),
                  LargeButton(
                    label: "confirme_modification".tr,
                    onPressed: () {},
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
