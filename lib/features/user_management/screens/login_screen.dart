import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/app_route.dart';
import 'package:delivery_user_app/features/user_management/controllers/auth_controller.dart';
import 'package:delivery_user_app/features/user_management/controllers/custom_fields_controllers.dart';
import 'package:delivery_user_app/features/user_management/models/login_model.dart';
import 'package:delivery_user_app/features/user_management/screens/sign_up_screen.dart';
import 'package:delivery_user_app/features/user_management/widgets/custom_fields.dart';
import 'package:delivery_user_app/features/user_management/widgets/large_button.dart';
import 'package:delivery_user_app/shared/screens/navigation_page.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});

  final CustomTextFormFieldController usernameController =
      CustomTextFormFieldController();
  final CustomTextFormFieldController passwordController =
      CustomTextFormFieldController();
  final AuthController authController = Get.put(AuthController());
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: SizedBox(
            height: fullHeight(context),
            width: fullWidth(context),
            child: Stack(
              children: [
                Positioned(
                    left: 0,
                    bottom: fullHeight(context) * 0.80,
                    child: Image.asset("assets/images/pattern2.png")),
                Positioned(
                  top: fullWidth(context) * 0.5,
                  right: fullWidth(context) * 0.1,
                  child: SizedBox(
                    height: fullHeight(context) * .9,
                    width: fullWidth(context) * .8,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "logToYourAccount".tr,
                          style: GoogleFonts.inter(
                            fontSize: 34,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(
                          height: fullHeight(context) * 0.01,
                        ),
                        Text(
                          "connectPlease".tr,
                          style: GoogleFonts.inter(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                              color: Color(0xFF878787)),
                        ),
                        SizedBox(
                          height: fullHeight(context) * 0.02,
                        ),
                        CustomTextFormField(
                          textFieldLabel: "phoneNumber".tr,
                          isPassword: false,
                          customController: usernameController,
                        ),
                        SizedBox(
                          height: fullHeight(context) * 0.02,
                        ),
                        CustomTextFormField(
                          textFieldLabel: "password".tr,
                          isPassword: true,
                          customController: passwordController,
                        ),
                        SizedBox(
                          height: fullHeight(context) * 0.03,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                            child: Text(
                              "forgotPassword".tr,
                              style: GoogleFonts.inter(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: AppConstants.customOrange),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: fullHeight(context) * 0.03,
                        ),
                        LargeButton(
                          label: "login".tr,
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              print('-------------------------------------');
                              print(usernameController
                                  .textEditingController.text);
                              print(passwordController
                                  .textEditingController.text);
                              LoginModel loginModel = LoginModel(
                                username: usernameController
                                    .textEditingController.text,
                                password: passwordController
                                    .textEditingController.text,
                              );
                              print("eee");
                              print(loginModel.username);

                              await authController.login(loginModel, context);
                            } else {
                              // Si le formulaire n'est pas valide, affichez un SnackBar
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  backgroundColor: Colors.red,
                                  content:
                                      Text("Le formulaire n'est pas valide."),
                                ),
                              );
                            }
                          },
                        ),
                        SizedBox(
                          height: fullHeight(context) * 0.02,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: InkWell(
                            //
                            onTap: () => {
                              Get.to(SignUpScreen()),
                            },

                            //onTap: g,
                            child: Text(
                              "create_account".tr,
                              style: GoogleFonts.inter(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: AppConstants.customOrange),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
