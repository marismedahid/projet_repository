import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../shared/widgets/custom_button_widget.dart';
import '../../app_routes.dart';

class DescriptionScreen extends StatefulWidget {
  final String? title;
  final String? subtitle;
  final String? imagePath;
  const DescriptionScreen({Key? key, this.title, this.subtitle, this.imagePath})
      : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<DescriptionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(
          horizontal: fullWidth(context) * 0.05,
          vertical: fullHeight(context) * 0.1),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: fullHeight(context) * 0.05),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          widget.imagePath != null
              ? Image.asset(
                  'assets/images/logo.png',
                  height: fullHeight(context) * 0.1,
                )
              : Container(),
          if (widget.title != null)
            Padding(
              padding:
                  EdgeInsets.symmetric(vertical: fullHeight(context) * 0.02),
              child: Text(
                  textAlign: TextAlign.center,
                  widget.title!,
                  style: TextStyle(
                      wordSpacing: 1,
                      color: Colors.black,
                      fontSize: calculateSize(context, 0.02))),
            ),
          if (widget.subtitle != null)
            Text(
                textAlign: TextAlign.center,
                widget.subtitle!,
                style: TextStyle(
                    wordSpacing: 1,
                    color: Colors.black,
                    fontSize: calculateSize(context, 0.015))),
          Card(
              margin:
                  EdgeInsets.symmetric(vertical: fullHeight(context) * 0.02),
              color: Colors.white,
              shape: const RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.all(Radius.circular(40))),
              elevation: 2,
              shadowColor: Colors.white,
              child: Container(
                  width: fullWidth(context) * 0.85,
                  height: fullHeight(context) * 0.45,
                  padding: EdgeInsets.symmetric(
                      horizontal: fullWidth(context) * 0.05,
                      vertical: fullHeight(context) * 0.03),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'deleteAccountReasonDescription'.tr,
                          style: TextStyle(
                            color: Color(0xFF03A9F4),
                            fontSize: calculateSize(context, 0.015),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: fullHeight(context) * 0.02),
                          child: _textArea(),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: fullHeight(context) * 0.02),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomeButtonWidget(
                                    title: 'back'.tr,
                                    heightPercentage: 0.055,
                                    width: fullWidth(context) * 0.35,
                                    fontSize: calculateSize(context, 0.015),
                                    onTap: () {
                                      Get.back();
                                    }),
                                CustomeButtonWidget(
                                    title: 'next',
                                    buttonColor: AppConstants.primaryColor,
                                    titleColor: Colors.white,
                                    heightPercentage: 0.055,
                                    width: fullWidth(context) * 0.35,
                                    fontSize: calculateSize(context, 0.015),
                                    onTap: () {
                                      Get.toNamed(UserManagementRoutes
                                          .confirmDeleteAccount);
                                    })
                              ]),
                        )
                      ],
                    ),
                  )))
        ]),
      ),
    ));
  }

  _textArea() {
    return TextField(
      controller: TextEditingController(),
      maxLines: 7,
      decoration: InputDecoration(
        hintText: 'hintText'.tr,
        hintStyle: TextStyle(
            color: Colors.grey[600], fontSize: calculateSize(context, 0.013)),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide:
              BorderSide(color: AppConstants.primaryColor.withOpacity(0.39)),
        ),
      ),
    );
  }
}
