import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/app_routes.dart';
import 'package:delivery_user_app/features/user_management/controllers/auth_controller.dart';
import 'package:delivery_user_app/features/user_management/models/reason_model.dart';
import 'package:delivery_user_app/shared/widgets/custom_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainScreen extends StatefulWidget {
  final String? title;
  final String? subtitle;
  final String? imagePath;
  const MainScreen({Key? key, this.title, this.subtitle, this.imagePath})
      : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  AuthController _authController = Get.put(AuthController());
  dynamic _selectedReason;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(
          horizontal: fullWidth(context) * 0.05,
          vertical: fullHeight(context) * 0.1),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: fullHeight(context) * 0.05),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          widget.imagePath != null
              ? Image.asset(
                  'assets/images/logo.png',
                  height: fullHeight(context) * 0.1,
                )
              : Container(),
          if (widget.title != null)
            Padding(
              padding:
                  EdgeInsets.symmetric(vertical: fullHeight(context) * 0.02),
              child: Text(
                  textAlign: TextAlign.center,
                  widget.title!,
                  style: TextStyle(
                      wordSpacing: 1,
                      color: Colors.black,
                      fontSize: calculateSize(context, 0.02))),
            ),
          if (widget.subtitle != null)
            Text(
                textAlign: TextAlign.center,
                widget.subtitle!,
                style: TextStyle(
                    wordSpacing: 1,
                    color: Colors.black,
                    fontSize: calculateSize(context, 0.015))),
          Card(
              margin:
                  EdgeInsets.symmetric(vertical: fullHeight(context) * 0.02),
              color: Colors.white,
              shape: const RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.all(Radius.circular(40))),
              elevation: 2,
              shadowColor: Colors.white,
              child: Container(
                  width: fullWidth(context) * 0.85,
                  height: fullHeight(context) * 0.55,
                  padding: EdgeInsets.symmetric(
                      horizontal: fullWidth(context) * 0.05,
                      vertical: fullHeight(context) * 0.03),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'deleteAccountReason'.tr,
                          style: TextStyle(
                            color: Color(0xFF03A9F4),
                            fontSize: calculateSize(context, 0.015),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: fullHeight(context) * 0.01),
                          height: fullHeight(context) * 0.37,
                          child: ListView.builder(
                            itemCount: _authController.reasons.length,
                            itemBuilder: (context, index) {
                              return _reasonItem(
                                  item: _authController.reasons[index]);
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: fullHeight(context) * 0.02),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomeButtonWidget(
                                    radius: 15,
                                    title: 'back'.tr,
                                    heightPercentage: 0.055,
                                    width: fullWidth(context) * 0.35,
                                    fontSize: calculateSize(context, 0.015),
                                    onTap: () {
                                      Get.back();
                                    }),
                                CustomeButtonWidget(
                                    radius: 15,
                                    title: 'next',
                                    buttonColor: AppConstants.primaryColor,
                                    titleColor: Colors.white,
                                    heightPercentage: 0.055,
                                    width: fullWidth(context) * 0.35,
                                    fontSize: calculateSize(context, 0.015),
                                    onTap: () {
                                      _selectedReason ==
                                              _authController.reasons.last
                                          ? Get.toNamed(UserManagementRoutes
                                              .deletionReasonDescription)
                                          : Get.toNamed(UserManagementRoutes
                                              .confirmDeleteAccount);
                                    })
                              ]),
                        )
                      ],
                    ),
                  )))
        ]),
      ),
    ));
  }

  Widget _reasonItem({required ReasonModel item}) {
    RxBool _chosenReason = false.obs;
    return Container(
      height: fullHeight(context) * 0.06,
      padding: EdgeInsets.symmetric(horizontal: fullWidth(context) * 0.02),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.grey[200]!),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            item.body ?? '',
            style: TextStyle(
              color: Colors.grey[700],
              fontSize: calculateSize(context, 0.015),
              fontWeight: FontWeight.w500,
            ),
          ),
          GestureDetector(
            onTap: () {
              if (_chosenReason.value)
                _chosenReason.value = false;
              else {
                _chosenReason.value = true;
                _selectedReason = item;
              }
            },
            child: Obx(
              () => _chosenReason.value
                  ? Icon(
                      Icons.check_circle_rounded,
                      size: fullWidth(context) * 0.05,
                      color: AppConstants.primaryColor,
                    )
                  : Icon(
                      Icons.circle_outlined,
                      size: fullWidth(context) * 0.05,
                      color: AppConstants.primaryColor,
                    ),
            ),
          )
        ],
      ),
    );
  }
}
