import 'dart:developer';

import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/app_routes.dart';
import 'package:delivery_user_app/features/user_management/controllers/user_controller.dart';
import 'package:delivery_user_app/features/user_management/models/user_model.dart';
import 'package:delivery_user_app/features/user_management/widgets/text_form_field_widget.dart';
import 'package:delivery_user_app/shared/widgets/custom_button_widget.dart';
import 'package:delivery_user_app/shared/widgets/date_picker_widget.dart';
import 'package:delivery_user_app/shared/widgets/dropdown_button_widget.dart';
import 'package:delivery_user_app/shared/widgets/loading_button_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class CompleteProfileScreen extends StatefulWidget {
  final String title;
  final String subtitle;
  final String? imagePath;
  const CompleteProfileScreen(
      {Key? key, required this.title, required this.subtitle, this.imagePath})
      : super(key: key);

  @override
  _CompleteProfileScreenState createState() => _CompleteProfileScreenState();
}

class _CompleteProfileScreenState extends State<CompleteProfileScreen> {
  UserController _userController = Get.put(UserController());
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late DateTime _selectedDate = DateTime.now();
  final _genders = ['male'.tr, 'female'.tr];
  late String? _gender;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.symmetric(
              horizontal: fullWidth(context) * 0.13,
              vertical: fullHeight(context) * 0.1),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding:
                    EdgeInsets.symmetric(vertical: fullHeight(context) * 0.05),
                child: Column(children: [
                  widget.imagePath != null
                      ? Image.asset(
                          'assets/images/logo.png',
                          height: fullHeight(context) * 0.1,
                        )
                      : Container(),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: fullHeight(context) * 0.02),
                    child: Text(
                        textAlign: TextAlign.center,
                        widget.title,
                        style: TextStyle(
                            wordSpacing: 1,
                            color: Colors.black,
                            fontSize: calculateSize(context, 0.02))),
                  ),
                  Text(
                      textAlign: TextAlign.center,
                      widget.subtitle,
                      style: TextStyle(
                          wordSpacing: 1,
                          color: Colors.black,
                          fontSize: calculateSize(context, 0.015))),
                ]),
              ),
              Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextFormFieldWidget(
                        enableBorder: true,
                        obscureText: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        controller: _userController.firstNameTextController,
                        labelText: 'firstName'.tr,
                        suffixIcon: Icon(Icons.person_2_outlined,
                            color: AppConstants.primaryColor),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'emptyField'.tr;
                          }
                          return null;
                        }),
                    TextFormFieldWidget(
                        enableBorder: true,
                        obscureText: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        controller: _userController.lastNameTextController,
                        labelText: 'lastName'.tr,
                        suffixIcon: Icon(Icons.person_2_outlined,
                            color: AppConstants.primaryColor),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'emptyField'.tr;
                          }
                          return null;
                        }),
                    TextFormFieldWidget(
                        enableBorder: true,
                        obscureText: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        controller: _userController.addressTextController,
                        labelText: 'address'.tr,
                        suffixIcon: Icon(Icons.location_history_outlined,
                            color: AppConstants.primaryColor),
                        validator: (value) {
                          return null;
                        }),
                    Container(
                      margin: EdgeInsets.symmetric(
                          vertical: fullHeight(context) * 0.02),
                      child: DropdownButtonWidget(
                        hintText: 'gender'.tr,
                        borderSideColor: AppConstants.primaryColor,
                        fillColor: Colors.transparent,
                        items: _genders,
                        onChanged: (newValue) {
                          if (newValue == _genders.first)
                            _gender = _userController.genders.first;
                          else
                            _gender = _userController.genders.last;
                        },
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: fullWidth(context) * 0.35,
                            child: DropdownButtonWidget(
                              hintText: 'country'.tr,
                              borderSideColor: AppConstants.primaryColor,
                              fillColor: Colors.transparent,
                              items: [],
                              onChanged: (newValue) {
                                log('$newValue');
                              },
                            )),
                        SizedBox(
                            width: fullWidth(context) * 0.35,
                            child: DropdownButtonWidget(
                              hintText: 'city'.tr,
                              borderSideColor: AppConstants.primaryColor,
                              fillColor: Colors.transparent,
                              items: [],
                              onChanged: (newValue) {
                                log('$newValue');
                              },
                            )),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: fullHeight(context) * 0.02),
                      child: DatePickerWidget(
                        hintText: 'birthDate'.tr,
                        width: fullWidth(context),
                        selectedDate: DateTime.now(),
                        selectDate: (BuildContext context) async {
                          final DateTime? pickedDate = await showDatePicker(
                            context: context,
                            locale: Get.locale,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1960),
                            lastDate: DateTime.now(),
                          );

                          if (pickedDate != null &&
                              pickedDate != _selectedDate) {
                            setState(() {
                              _selectedDate = pickedDate;
                            });
                          }
                        },
                      ),
                    ),
                    Obx(
                      () => _userController.isLoading.value
                          ? const LoadingButtonWidget()
                          : CustomeButtonWidget(
                              title: 'confirm'.tr,
                              onTap: () async {
                                await _userController.updateProfile(
                                    UserModel(
                                        firstName: _userController
                                            .firstNameTextController.text,
                                        lastName: _userController
                                            .lastNameTextController.text,
                                        gender: _gender,
                                        address: _userController
                                            .addressTextController.text,
                                        kcId: standardDateFormatter
                                            .format(_selectedDate)),
                                    context);
                                if (_userController.currentUser != null) {
                                  UserController.setFirstLogin(false);
                                  Get.offAllNamed(UserManagementRoutes.profile);
                                }
                              },
                            ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
