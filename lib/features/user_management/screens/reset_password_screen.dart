import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/shared/widgets/custom_button_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../shared/widgets/loading_button_widget.dart';
import '../../../shared/widgets/pin_input_widget.dart';
import '../app_routes.dart';
import '../controllers/auth_controller.dart';
import '../../../shared/widgets/text_form_field_widget.dart';
import '../models/update_password_model.dart';

class ResetPasswordScreen extends StatefulWidget {
  final String title;
  final String subtitle;
  final String? imagePath;
  const ResetPasswordScreen(
      {Key? key, required this.title, required this.subtitle, this.imagePath})
      : super(key: key);

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  AuthController _authController = Get.put(AuthController());
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.symmetric(
              horizontal: fullWidth(context) * 0.13,
              vertical: fullHeight(context) * 0.1),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding:
                    EdgeInsets.symmetric(vertical: fullHeight(context) * 0.05),
                child: Column(children: [
                  widget.imagePath != null
                      ? Image.asset(
                          'assets/images/logo.png',
                          height: fullHeight(context) * 0.1,
                        )
                      : Container(),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: fullHeight(context) * 0.02),
                    child: Text(
                        textAlign: TextAlign.center,
                        widget.title,
                        style: TextStyle(
                            wordSpacing: 1,
                            color: Colors.black,
                            fontSize: calculateSize(context, 0.02))),
                  ),
                  Text(
                      textAlign: TextAlign.center,
                      widget.subtitle,
                      style: TextStyle(
                          wordSpacing: 1,
                          color: Colors.black,
                          fontSize: calculateSize(context, 0.015))),
                ]),
              ),
              Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                        padding:
                            EdgeInsets.only(top: fullHeight(context) * 0.02),
                        child: Text('validationCode'.tr,
                            style: TextStyle(
                                color: Colors.grey[600],
                                fontSize: calculateSize(context, 0.012)))),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: fullHeight(context) * 0.02),
                        child: PinInputWidget(
                            controller: _authController.pinInputController,
                            length: 4)),
                    TextFormFieldWidget(
                        enableBorder: true,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        textInputAction: TextInputAction.next,
                        controller: _authController.passwordTextController,
                        labelText: 'password'.tr,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'emptyField'.tr;
                          } else if (value.length < 4) {
                            return 'incorrectPassword'.tr;
                          }
                          return null;
                        }),
                    TextFormFieldWidget(
                        enableBorder: true,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        textInputAction: TextInputAction.next,
                        controller:
                            _authController.confirmPasswordTextController,
                        labelText: 'confirmPassword'.tr,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'emptyField'.tr;
                          } else if (_authController
                                  .passwordTextController.text !=
                              _authController
                                  .confirmPasswordTextController.text) {
                            return 'passwordsDoNotMatch'.tr;
                          }
                          return null;
                        }),
                    Obx(
                      () => Padding(
                        padding:
                            EdgeInsets.only(top: fullHeight(context) * 0.02),
                        child: _authController.isLoading.value
                            ? LoadingButtonWidget()
                            : CustomeButtonWidget(
                                radius: 15,
                                title: 'continue'.tr,
                                onTap: () async {
                                  if (_formKey.currentState!.validate()) {
                                    await _authController
                                        .updatePassword(
                                            UpdatePasswordModel(
                                                code: _authController
                                                    .pinInputController.text,
                                                password: _authController
                                                    .passwordTextController
                                                    .text),
                                            context)
                                        .then((value) {
                                      if (value)
                                        Get.offNamed(
                                            UserManagementRoutes.login);
                                    });
                                  }
                                },
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
