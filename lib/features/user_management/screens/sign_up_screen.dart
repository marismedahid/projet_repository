import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/controllers/auth_controller.dart';
import 'package:delivery_user_app/features/user_management/controllers/custom_fields_controllers.dart';
import 'package:delivery_user_app/features/user_management/models/user_model.dart';
import 'package:delivery_user_app/features/user_management/widgets/custom_fields.dart';
import 'package:delivery_user_app/features/user_management/widgets/large_button.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'package:google_fonts/google_fonts.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen({super.key});

  final CustomTextFormFieldController usernameController =
      CustomTextFormFieldController();
  final CustomTextFormFieldController prenomController =
      CustomTextFormFieldController();
  final CustomTextFormFieldController phoneController =
      CustomTextFormFieldController();
  final CustomTextFormFieldController emailController =
      CustomTextFormFieldController();
  final CustomDropdownController genre = CustomDropdownController();
  final CustomTextFormFieldController passwordController =
      CustomTextFormFieldController();
  final CustomTextFormFieldController confirmpasswordcontroller =
      CustomTextFormFieldController();
  final AuthController authController = Get.put(AuthController());
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: SizedBox(
            height: fullHeight(context),
            width: fullWidth(context),
            child: Stack(
              children: [
                Positioned(
                    left: fullWidth(context) * .3,
                    bottom: fullHeight(context) * 0.8,
                    child: Image.asset("assets/images/pattern.png")),
                Positioned(
                  top: fullHeight(context) * 0.1,
                  right: fullWidth(context) * 0.1,
                  child: SizedBox(
                      height: fullHeight(context) * .9,
                      width: fullWidth(context) * .8,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "logToYourAccount".tr,
                              style: GoogleFonts.inter(
                                fontSize: 34,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(
                              height: fullHeight(context) * 0.01,
                            ),
                            Text(
                              "connectPlease".tr,
                              style: GoogleFonts.inter(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w300,
                                  color: Color(0xFF878787)),
                            ),
                            SizedBox(
                              height: fullHeight(context) * 0.02,
                            ),
                            CustomTextFormField(
                              textFieldLabel: "lastName".tr,
                              isPassword: false,
                              customController: usernameController,
                            ),
                            SizedBox(
                              height: fullHeight(context) * 0.02,
                            ),
                            // CustomTextFormField(
                            //   textFieldLabel: "firstName".tr,
                            //   isPassword: false,
                            //   customController: prenomController,
                            // ),
                            SizedBox(
                              height: fullHeight(context) * 0.02,
                            ),
                            CustomTextFormField(
                              textFieldLabel: "Phone number".tr,
                              isPassword: false,
                              customController: phoneController,
                            ),
                            SizedBox(
                              height: fullHeight(context) * 0.02,
                            ),
                            CustomTextFormField(
                              textFieldLabel: "email".tr,
                              isPassword: false,
                              customController: emailController,
                            ),
                            // CustomDropdown(
                            //   textFieldLabel: "gender".tr,
                            //   customDropdownController:
                            //       genre,
                            // ),
                            SizedBox(
                              height: fullHeight(context) * 0.02,
                            ),
                            CustomTextFormField(
                              textFieldLabel: "password".tr,
                              isPassword: true,
                              customController: passwordController,
                            ),
                            SizedBox(
                              height: fullHeight(context) * 0.02,
                            ),
                            CustomTextFormField(
                              textFieldLabel: "confirmPassword".tr,
                              isPassword: true,
                              customController: confirmpasswordcontroller,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Veuillez entrer du texte';
                                } else if (value !=
                                    passwordController
                                        .textEditingController.text) {
                                  return 'Mots de passe ne correspondent pas'
                                      .tr;
                                }
                                return null;
                              },
                            ),
                            SizedBox(
                              height: fullHeight(context) * 0.02,
                            ),
                            LargeButton(
                              label: "signUp".tr,
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  UserModel model = UserModel(
                                      username: usernameController
                                          .textEditingController.text,
                                      email: emailController
                                          .textEditingController.text,
                                      phoneNumber: phoneController
                                          .textEditingController.text,
                                      password: passwordController
                                          .textEditingController.text);
                                  await authController.register(model, context);
                                } else {
                                  print('wwwwwwwwwwwwwwwwwww' +
                                      '$passwordController');
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      backgroundColor: Colors.red,
                                      content: Text(
                                          "Le formulaire n'est pas valide.".tr),
                                    ),
                                  );
                                }
                              },
                            ),
                          ],
                        ),
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
