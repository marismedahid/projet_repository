import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/widgets/text_form_field_widget.dart';
import 'package:delivery_user_app/shared/widgets/clickable_text_widget.dart';
import 'package:delivery_user_app/shared/widgets/custom_button_widget.dart';
import 'package:delivery_user_app/shared/widgets/loading_button_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import '../app_routes.dart';
import '../controllers/auth_controller.dart';
import '../widgets/confirmation_popup.dart';

class ForgotPasswordScreen extends StatefulWidget {
  final String title;
  final String subtitle;
  final String? imagePath;
  const ForgotPasswordScreen({
    super.key,
    required this.title,
    required this.subtitle,
    this.imagePath,
  });

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  AuthController _authController = Get.put(AuthController());
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.symmetric(
              horizontal: fullWidth(context) * 0.13,
              vertical: fullHeight(context) * 0.1),
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.always,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: fullHeight(context) * 0.05),
                  child: Column(children: [
                    widget.imagePath != null
                        ? Image.asset(
                            'assets/images/logo.png',
                            height: fullHeight(context) * 0.1,
                          )
                        : Container(),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: fullHeight(context) * 0.02),
                      child: Text(
                          textAlign: TextAlign.center,
                          widget.title,
                          style: TextStyle(
                              wordSpacing: 1,
                              color: Colors.black,
                              fontSize: calculateSize(context, 0.02))),
                    ),
                    Text(
                        textAlign: TextAlign.center,
                        widget.subtitle,
                        style: TextStyle(
                            wordSpacing: 1,
                            color: Colors.black,
                            fontSize: calculateSize(context, 0.015))),
                  ]),
                ),
                Obx(
                  () => Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: fullHeight(context) * 0.05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextFormFieldWidget(
                            enableBorder: true,
                            obscureText: false,
                            textInputAction: TextInputAction.next,
                            keyboardType:
                                _authController.forgotPasswordByPhone.value
                                    ? TextInputType.number
                                    : TextInputType.emailAddress,
                            controller:
                                _authController.forgotPasswordByPhone.value
                                    ? _authController.phoneTextController
                                    : _authController.emailTextController,
                            maxLength: null,
                            labelText:
                                _authController.forgotPasswordByPhone.value
                                    ? 'phoneNumber'.tr
                                    : 'email'.tr,
                            suffixIcon: Icon(
                                _authController.forgotPasswordByPhone.value
                                    ? Icons.call_outlined
                                    : Icons.email_outlined,
                                color: AppConstants.primaryColor),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'emptyField'.tr;
                              } else if (_authController
                                      .forgotPasswordByPhone.value &&
                                  !AppConstants.phoneRegExp.hasMatch(value)) {
                                return 'invalidPhoneNumber'.tr;
                              }
                              return null;
                            }),
                        Padding(
                          padding:
                              EdgeInsets.only(top: fullHeight(context) * 0.02),
                          child: _authController.isLoading.value
                              ? LoadingButtonWidget()
                              : CustomeButtonWidget(
                                  title: 'continue'.tr,
                                  onTap: () async {
                                    if (_formKey.currentState!.validate()) {
                                      await _authController
                                          .resetPassword(
                                              _authController
                                                  .emailTextController.text,
                                              context)
                                          .then((value) {
                                        if (value) {
                                          confirmationPopup(
                                            twoOptions: false,
                                            body:
                                                'checkInboxForConfirmationCode'
                                                    .tr,
                                            context: context,
                                            confirmOnPressed: () {
                                              Get.toNamed(UserManagementRoutes
                                                  .resetPassword);
                                            },
                                          );
                                        }
                                      });
                                    }
                                  },
                                ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: fullHeight(context) * 0.02),
                          child: Align(
                              alignment: Alignment.centerRight,
                              child: ClickableTextWidget(
                                  onTap: () => _authController
                                      .forgotPasswordByPhone
                                      .toggle(),
                                  text: _authController
                                          .forgotPasswordByPhone.value
                                      ? 'tryWithEmail'.tr
                                      : 'tryWithPhoneNumber'.tr)),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
