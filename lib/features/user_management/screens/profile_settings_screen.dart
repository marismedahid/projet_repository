import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/widgets/page_title_widget.dart';
import 'package:delivery_user_app/features/user_management/widgets/profile_list_item_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfileSettingsScreen extends StatelessWidget {
  const ProfileSettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Stack(
        children: [
          Positioned(
              left: fullWidth(context) * .35,
              bottom: fullHeight(context) * 0.7,
              child: Image.asset("assets/images/pattern.png")),
          Padding(
            padding: EdgeInsets.only(
              top: fullHeight(context) * .05,
              left: fullWidth(context) * .09,
              right: fullWidth(context) * .05,
              bottom: fullHeight(context) * .005,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PageTitle(
                    title: "settings".tr,
                  ),
                  SizedBox(
                    height: calculateSize(context, 0.03),
                  ),
                  Text(
                    "profile".tr,
                    style: GoogleFonts.inter(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF878787)),
                  ),
                  SizedBox(
                    height: calculateSize(context, 0.03),
                  ),
                  ProfileListItem(
                    label: "Alerte des notifications",
                    onPressed: () {},
                    withSwitcher: true,
                  ),
                  SizedBox(
                    height: calculateSize(context, 0.03),
                  ),
                  ProfileListItem(
                    label: "Localisation",
                    onPressed: () {},
                    withSwitcher: true,
                  ),
                  SizedBox(
                    height: calculateSize(context, 0.03),
                  ),
                  ProfileListItem(
                    label: "Langue",
                    secondLabel: "Français",
                    onPressed: () {},
                    withForward: true,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
