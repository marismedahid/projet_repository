


import 'package:flutter/material.dart';

class HorizontalDivider extends StatelessWidget {
  const HorizontalDivider({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      height: 2,
      // width: fullWidth(context)*0.8,
      color: Color(0xFFEDEDED),
    );
  }
}
