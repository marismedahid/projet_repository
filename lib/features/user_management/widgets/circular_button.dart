import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/cupertino.dart';

class CircularButton extends StatelessWidget {
  const CircularButton({
    super.key,
    required this.icon,
    required this.onPressed,
  });
  final Icon icon;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed();
      },
      child: Container(
        padding: EdgeInsets.all(calculateSize(context, 0.01)),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 1, color: Color(0xFFEDEDED))),
        child: icon,
      ),
    );
  }
}
