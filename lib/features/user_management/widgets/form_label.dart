import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FormLabel extends StatelessWidget {
  const FormLabel({
    super.key,
    required this.text,
  });
  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: GoogleFonts.inter(
          fontSize: 14, fontWeight: FontWeight.w500, color: AppConstants.black),
    );
  }
}
