import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget Lodingimage() => Center(
      child: Image.asset(
        "assets/images/loading1.gif",
        width: Get.size.width * 0.2,
        height: Get.size.height * 0.1,
      ),
    );
