import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/controllers/custom_fields_controllers.dart';
import 'package:delivery_user_app/features/user_management/widgets/form_label.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomDropdown extends StatelessWidget {
  const CustomDropdown({
    super.key,
    required this.textFieldLabel,
    required this.customDropdownController,
  });
  final String textFieldLabel;
  final CustomDropdownController customDropdownController;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FormLabel(
          text: textFieldLabel,
        ),
        SizedBox(
          height: fullHeight(context) * .01,
        ),
        Container(
          padding: EdgeInsets.symmetric(
              vertical: 2, horizontal: fullWidth(context) * .02),
          height: fullHeight(context) * 0.07,
          decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                color: Color(0xFFD6D6D6),
              ),
              borderRadius: BorderRadius.circular(8)),
          child: Obx(
            () => DropdownButton(
              focusColor: Colors.transparent,
              underline: SizedBox(),
              isExpanded: true,
              value: customDropdownController.value.value,
              onChanged: (value) {
                customDropdownController.setValue(value!);
              },
              items: [
                DropdownMenuItem(
                    value: 1,
                    child: Text(
                      "male".tr,
                      style: GoogleFonts.inter(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: AppConstants.black),
                    )),
                DropdownMenuItem(
                    value: 2,
                    child: Text(
                      "female".tr,
                      style: GoogleFonts.inter(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: AppConstants.black),
                    )),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class CustomTextFormField extends StatelessWidget {
  CustomTextFormField(
      {super.key,
      required this.textFieldLabel,
      this.isPassword = false,
      required this.customController,
      this.initialValue = "",
      this.validator});

  final String textFieldLabel;
  final bool isPassword;
  final CustomTextFormFieldController customController;
  final String initialValue;
  final String? Function(String?)? validator;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FormLabel(
          text: textFieldLabel,
        ),
        SizedBox(
          height: fullHeight(context) * .01,
        ),
        Container(
            padding: EdgeInsets.symmetric(
                vertical: 2, horizontal: fullWidth(context) * .02),
            height: fullHeight(context) * 0.07,
            decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Color(0xFFD6D6D6),
                ),
                borderRadius: BorderRadius.circular(8)),
            child: Obx(
              () => customController.alwaysTrue.value
                  ? TextFormField(
                      validator: validator ??
                          (value) {
                            if (value == null || value.isEmpty) {
                              return ' Veuillez entrer du texte'.tr;
                            }
                            return null;
                          },
                      controller: customController.textEditingController,
                      //initialValue: initialValue,

                      obscureText:
                          isPassword && customController.showPassword.value,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: isPassword
                              ? GestureDetector(
                                  onTap: () {
                                    customController.toggleShowPassword();
                                  },
                                  child: Icon(
                                    // Removed Obx here
                                    customController.showPassword.value
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                    color: AppConstants.black,
                                  ),
                                )
                              : null),
                    )
                  : SizedBox(),
            )),
      ],
    );
  }
}
