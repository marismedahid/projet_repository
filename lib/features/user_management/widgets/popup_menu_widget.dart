import 'package:flutter/material.dart';
import 'package:get/get.dart';

// import '../app_routes.dart';
import '../controllers/auth_controller.dart';
import 'confirmation_popup.dart';

enum Item { logOut, deleteAccount }

class PopupMenuWidget extends StatefulWidget {
  const PopupMenuWidget({super.key});

  @override
  State<PopupMenuWidget> createState() => _PopupMenuWidgetState();
}

class _PopupMenuWidgetState extends State<PopupMenuWidget> {
  AuthController _authController = Get.put(AuthController());
  Item? selectedMenu;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      initialValue: selectedMenu,
      onSelected: (Item item) {
        setState(() {
          selectedMenu = item;
        });
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<Item>>[
        PopupMenuItem<Item>(
          value: Item.logOut,
          child: Text('disconnect'.tr),
          onTap: () async {
            await confirmationPopup(
                body: 'logOut'.tr,
                context: context,
                twoOptions: true,
                confirmOnPressed: () async {
                  await _authController.logout();
                });
          },
        ),
        PopupMenuItem<Item>(
          value: Item.deleteAccount,
          child: Text('deleteAccount'.tr),
          onTap: () async {
            // await confirmationPopup(
            //     body: 'deleteAccountQuestion'.tr,
            //     context: context,
            //     twoOptions: true,
            //     confirmOnPressed: () =>
            //         Get.toNamed(UserManagementRoutes.deleteAccount));
          },
        ),
      ],
    );
  }
}
