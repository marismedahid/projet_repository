import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TextFormFieldWidget extends StatefulWidget {
  final String labelText;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final TextEditingController controller;
  final String? Function(String?)? validator;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Color? suffixIconColor;
  final int? maxLength;
  final bool obscureText;
  final bool enableBorder;
  final Color? borderColor;
  TextFormFieldWidget({
    Key? key,
    required this.controller,
    required this.labelText,
    required this.textInputAction,
    this.prefixIcon,
    this.suffixIcon,
    this.suffixIconColor,
    required this.validator,
    required this.keyboardType,
    required this.obscureText,
    this.maxLength,
    required this.enableBorder,
    this.borderColor,
  }) : super(key: key);

  @override
  _TextFormFieldWidgetState createState() => _TextFormFieldWidgetState();
}

class _TextFormFieldWidgetState extends State<TextFormFieldWidget> {
  bool _obscureText = true;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      maxLength: widget.maxLength,
      style: TextStyle(color: Colors.black),
      keyboardType: widget.keyboardType,
      obscureText: widget.obscureText ? _obscureText : false,
      decoration: InputDecoration(
          labelText: widget.labelText,
          labelStyle: TextStyle(
              color: Colors.grey[600], fontSize: calculateSize(context, 0.014)),
          hintStyle: TextStyle(
              color: Colors.grey[600], fontSize: calculateSize(context, 0.014)),
          hintText: 'hintText'.tr,
          enabledBorder: widget.enableBorder
              ? UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: widget.borderColor ?? AppConstants.primaryColor))
              : null,
          prefixIcon: widget.prefixIcon ?? null,
          suffixIcon: !widget.obscureText
              ? widget.suffixIcon ?? null
              : IconButton(
                  icon: Icon(
                      _obscureText ? Icons.visibility : Icons.visibility_off,
                      color:
                          widget.suffixIconColor ?? AppConstants.primaryColor),
                  onPressed: () {
                    setState(() {
                      _obscureText = !_obscureText;
                    });
                  },
                )),
      validator: widget.validator ??
          (value) {
            if (value == null || value.isEmpty) {
              return 'emptyField'.tr;
            }
            return null;
          },
    );
  }
}
