import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/widgets/circular_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class PageTitle extends StatelessWidget {
  const PageTitle({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircularButton(
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            size: calculateSize(context, 0.02),
            weight: 50,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        Spacer(),
        Text(
          title,
          style: GoogleFonts.inter(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: AppConstants.black),
        ),
        Spacer(),
      ],
    );
  }
}
