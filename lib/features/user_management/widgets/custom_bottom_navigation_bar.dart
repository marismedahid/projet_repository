import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/features/user_management/controllers/custom_bottom_navigation_bar_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  const CustomBottomNavigationBar({
    super.key,
    required this.controller,
  });
  final CustomBottomNavigationBarController controller;

  @override
  State<CustomBottomNavigationBar> createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    widget.controller.updateCurrentIndex(index);
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'home'.tr),
        BottomNavigationBarItem(
            icon: Icon(Icons.chat_bubble_outline_outlined),
            label: 'messages'.tr),
        BottomNavigationBarItem(
            icon: Icon(Icons.menu_sharp), label: 'orders'.tr),
        BottomNavigationBarItem(icon: Icon(Icons.person), label: 'profile'.tr),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: AppConstants.customOrange,
      unselectedItemColor: Color(0xFF818288),
      unselectedLabelStyle: TextStyle(color: Colors.black),
      onTap: _onItemTapped,
    );
  }
}
