import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/controllers/auth_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'leading_widget.dart';

class LargeButton extends StatelessWidget {
  LargeButton({
    super.key,
    required this.onPressed,
    required this.label,
  });
  final AuthController authController = Get.put(AuthController());

  final Function() onPressed;
  final String label;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: fullHeight(context) * 0.07,
        decoration: BoxDecoration(
            color: AppConstants.customOrange,
            borderRadius: BorderRadius.circular(100)),
        child: Obx(
          () => Center(
            child: authController.isLoading.value
                ? Lodingimage()
                : Text(
                    label,
                    style: GoogleFonts.inter(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFFFFFFFF)),
                  ),
          ),
        ),
      ),
    );
  }
}
