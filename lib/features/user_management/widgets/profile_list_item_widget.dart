import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/user_management/widgets/custom_switcher_widgte.dart';
import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

class ProfileListItem extends StatelessWidget {
  const ProfileListItem({
    super.key,
    this.icon,
    required this.label,
    required this.onPressed,
    this.withSwitcher = false,
    this.withForward = false,
    this.secondLabel,
  });

  final bool withSwitcher;
  final bool withForward;
  final Icon? icon;
  final String label;
  final String? secondLabel;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed();
      },
      child: Row(
        children: [
          if (icon != null)
            Container(
              height: calculateSize(context, 0.04),
              width: calculateSize(context, 0.04),
              decoration: BoxDecoration(
                color: Color(0xFFF5F5FF),
                borderRadius: BorderRadius.circular(10),
              ),
              child: icon,
            ),
          SizedBox(
            width: fullWidth(context) * 0.03,
          ),
          Text(
            label,
            style: GoogleFonts.inter(fontSize: 14, fontWeight: FontWeight.w500),
          ),
          Spacer(),
          if (secondLabel != null)
            Text(
              secondLabel!,
              style:
                  GoogleFonts.inter(fontSize: 14, fontWeight: FontWeight.w500),
            ),
          if (secondLabel != null)
            SizedBox(
              width: calculateSize(context, 0.02),
            ),
          if (withForward) Icon(Icons.keyboard_arrow_right),
          if (withSwitcher)
            CustomSwitcher(width: 60, controller: CustomSwitcherController())
        ],
      ),
    );
  }
}
