import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:get/get.dart';

Future<void> confirmationPopup(
    {required String body,
    required BuildContext context,
    String? imagePath,
    Function()? confirmOnPressed,
    double? imageWidth,
    required bool twoOptions,
    bool? barrierDismissible,
    double? imageHeight}) async {
  return showDialog(
    barrierDismissible: barrierDismissible ?? true,
    context: Get.context ?? context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(40.0),
        ),
        title: Image.asset(
          'assets/images/logo.png',
          width: fullWidth(context) * 0.05,
          height: fullHeight(context) * 0.03,
        ),
        content: Text(body,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: calculateSize(context, 0.015),
              color: HexColor("#516562"),
              fontWeight: FontWeight.w500,
            )),
        actionsAlignment: MainAxisAlignment.center,
        actionsPadding: EdgeInsets.symmetric(
          horizontal: fullWidth(context) * 0.03,
          vertical: fullHeight(context) * 0.01,
        ),
        actions: twoOptions
            ? [
                _buildElevatedButton('yes'.tr, confirmOnPressed),
                _buildElevatedButton('no'.tr, () => Get.back())
              ]
            : [_buildElevatedButton('continue'.tr, confirmOnPressed)],
      );
    },
  );
}

_buildElevatedButton(String text, Function()? onPressed) {
  return ElevatedButton(
    onPressed: onPressed,
    style: ElevatedButton.styleFrom(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      backgroundColor: HexColor("#62C6FF"),
      elevation: 0,
    ),
    child: Text(text),
  );
}
