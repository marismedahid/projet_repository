


import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomSwitcher extends StatelessWidget {
  const CustomSwitcher(
      {super.key, required this.width, required this.controller});
  final double width;
  final CustomSwitcherController controller;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        controller.switchIsActive();
      },
      child: Obx(
        () => AnimatedContainer(
          duration: Duration(milliseconds: 50),
          curve: Curves.easeInOut,
          alignment: controller.isActive.value
              ? Alignment.centerRight
              : Alignment.centerLeft,
          padding: EdgeInsets.symmetric(vertical: 3, horizontal: 3),
          width: width,
          height: 27,
          decoration: BoxDecoration(
              color: controller.isActive.value
                  ? Color(0xFF05B171)
                  : Color(0xFF818288),
              borderRadius: BorderRadius.circular(28)),
          child: Container(
            height: 20,
            width: 20,
            decoration:
                BoxDecoration(shape: BoxShape.circle, color: Colors.white),
          ),
        ),
      ),
    );
  }
}

class CustomSwitcherController extends GetxController {
  var isActive = true.obs;

  void setIsActive(bool value) {
    isActive.value = value;
  }

  void switchIsActive() {
    isActive.value = !isActive.value;
  }
}
