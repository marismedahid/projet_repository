import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';

class RoleCard extends StatefulWidget {
  final String title;
  final TextStyle? titleTextStyle;
  final Widget image;
  final double height;
  final double width;
  final Color? color;
  final double elevation;
  final double? borderRadius;
  final bool borderedSide;
  final Color? borderSideColor;
  final double? borderSideWidth;
  final double separator;
  final bool alignHorizontally;
  final Function()? onTap;

  RoleCard(
      {required this.title,
      this.titleTextStyle,
      required this.image,
      required this.height,
      required this.width,
      this.color,
      this.elevation = 0,
      this.borderRadius,
      this.borderedSide = false,
      this.borderSideColor,
      this.borderSideWidth,
      this.separator = 0,
      this.alignHorizontally = true,
      this.onTap});

  @override
  State<RoleCard> createState() => _RoleCardState();
}

class _RoleCardState extends State<RoleCard> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height,
      width: widget.width,
      child: Card(
        elevation: widget.elevation,
        shape: RoundedRectangleBorder(
          side: widget.borderedSide
              ? BorderSide(
                  color: widget.borderSideColor ?? Colors.black,
                  width: (widget.borderSideWidth ?? fullWidth(context) * 0.001))
              : BorderSide.none,
          borderRadius: BorderRadius.circular(widget.borderRadius ?? 15),
        ),
        color: widget.color ?? Colors.white,
        child: widget.alignHorizontally
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  widget.image,
                  SizedBox(width: widget.separator),
                  Text(widget.title, style: widget.titleTextStyle ?? null),
                ],
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  widget.image,
                  SizedBox(height: widget.separator),
                  Text(widget.title),
                ],
              ),
      ),
    );
  }
}
