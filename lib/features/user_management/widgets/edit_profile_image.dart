import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';

class EditProfileImageWidget extends StatelessWidget {
  const EditProfileImageWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: calculateSize(context, .15),
          height: calculateSize(context, .15),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.red,
              image: DecorationImage(
                  image: AssetImage(
                    'assets/images/profile.png',
                  ),
                  fit: BoxFit.cover)),
        ),
        Positioned(
            bottom: 0,
            right: 0,
            child: Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Color(0xFFF5F5FF), shape: BoxShape.circle),
                child: Icon(Icons.camera_alt)))
      ],
    );
  }
}
