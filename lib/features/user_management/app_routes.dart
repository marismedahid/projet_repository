import 'package:delivery_user_app/features/user_management/screens/edit_profile_screen.dart';
import 'package:delivery_user_app/features/user_management/screens/profile_settings_screen.dart';
import 'package:get/get.dart';

import 'screens/complete_profile_screen.dart';
import 'screens/delete_account/confirmation_screen.dart';
import 'screens/delete_account/description_screen.dart';
import 'screens/delete_account/main_screen.dart';
import 'screens/forgot_password_screen.dart';
import 'screens/login_screen.dart';
import 'screens/profile_screen.dart';
import 'screens/reset_password_screen.dart';
import 'screens/sign_up_screen.dart';

class UserManagementRoutes {
  static const String login = '/user-management';
  static const String signUp = '/user-management/sign-up';
  static const String forgotPassword = '/user-management/forgot-password';
  static const String resetPassword = '/user-management/reset-password';
  static const String completeProfile = '/user-management/complete-profile';
  static const String profile = '/user-management/profile';
  static const String deleteAccount = '/user-management/delete-account';

  static const String editProfile = '/user-management/edit-profile';
  static const String profileSettings = '/user-management/profile-settings';
  static const String confirmDeleteAccount =
      '/user-management/delete-account/confirm-delete-account';
  static const String deletionReasonDescription =
      '/user-management/delete-account/deletion-reason-description';

  static final List<GetPage> routes = [
    GetPage(
      name: login,
      page: () => LoginScreen(),
    ),
    GetPage(
      name: signUp,
      page: () => SignUpScreen(),
    ),
    GetPage(
        name: forgotPassword,
        page: () => ForgotPasswordScreen(
            title: 'forgotPasswordTitle'.tr,
            subtitle: 'forgotPasswordSubtitle'.tr)),
    GetPage(
        name: resetPassword,
        page: () => ResetPasswordScreen(
            title: 'resetPasswordTitle'.tr,
            subtitle: 'resetPasswordSubtitle'.tr)),
    GetPage(
        name: completeProfile,
        page: () => CompleteProfileScreen(
            title: 'completeProfileTitle'.tr,
            subtitle: 'completeProfileSubtitle'.tr)),
    GetPage(name: profile, page: () => ProfileScreen()),
    GetPage(name: deleteAccount, page: () => MainScreen()),
    GetPage(name: deletionReasonDescription, page: () => DescriptionScreen()),
    GetPage(name: confirmDeleteAccount, page: () => ConfirmationScreen()),
    GetPage(name: editProfile, page: () => EditProfileScreen()),
    GetPage(name: profileSettings, page: () => ProfileSettingsScreen()),
  ];
}
