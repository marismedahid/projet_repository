import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utils {
  setToken(String token, SharedPreferences sharedPreferences) async {
    sharedPreferences.setString('token', token);
  }

  Future<String> getToken(SharedPreferences sharedPreferences) async {
    if (sharedPreferences.containsKey('token')) {
      var _token = sharedPreferences.getString('token');
      return _token!;
    }
    return '';
  }

  getSnackBar(BuildContext context, String? message) {
    String exceptionMessage = message.toString();
    exceptionMessage = exceptionMessage.replaceAll('Exception: ', '');
    Get.snackbar(
      'error'.tr,
      exceptionMessage,
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: Colors.red,
      colorText: Colors.white,
      duration: const Duration(seconds: 3),
      borderRadius: 15.0,
      isDismissible: true,
      forwardAnimationCurve: Curves.easeOut,
      reverseAnimationCurve: Curves.easeIn,
    );
    // ScaffoldMessenger.of(Get.context ?? context).showSnackBar(SnackBar(
    //   backgroundColor: Colors.red,
    //   duration: Duration(seconds: 3),
    //   content: Text(exceptionMessage.tr, style: TextStyle(color: Colors.white)),
    // ));
  }

  getExceptionSnackBar(BuildContext context, Object e, String endPointName) {
    var message = e.toString().replaceFirst('Exception: ', '');
    getSnackBar(
        context,
        getExceptionLabelKey(
            message.replaceFirst('Exception: ', ''), endPointName));
  }

  static getExceptionLabelKey(String httpStatus, String methodName) {
    if (httpStatus == '304' ||
        httpStatus == '401' ||
        httpStatus == '403' ||
        httpStatus == '404' ||
        httpStatus == '409' ||
        httpStatus == '410' ||
        httpStatus == '500')
      return '$methodName$httpStatus';
    else
      return 'exception';
  }
}
