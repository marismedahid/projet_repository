import 'dart:convert';

import 'package:delivery_user_app/app_route.dart';
import 'package:delivery_user_app/shared/service/abstract_api_config.dart';
import 'package:get/get.dart';

import '../models/login_model.dart';
import '../models/login_response_model.dart';
import '../models/user_model.dart';
import '../models/update_password_model.dart';

class ApiAuth {
  AbstractApiConfig _apiConfig;

  ApiAuth(this._apiConfig);

  register(UserModel registerModel) async {
    try {
      _apiConfig.url = 'auth/register';
      _apiConfig.headers = {'Content-Type': 'application/json'};
      _apiConfig.body = jsonEncode(registerModel.toJson());
      final response = await _apiConfig.post();
      if (response.statusCode == 200 || response.statusCode == 201) {
        print('la connexion est reusi');

        return response.body;
      } else {
        throw Exception(response.statusCode);
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  login(LoginModel loginModel) async {
    try {
      _apiConfig.url = 'auth/login';
      _apiConfig.headers = {'Content-Type': 'application/json'};
      _apiConfig.body = jsonEncode(loginModel.toJson());
      final response = await _apiConfig.post();
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print('la connexion est reusi');
        Get.offAllNamed(AppRoutes.navigationPage);

        return LoginResponseModel.fromJson(jsonData);
      } else {
        throw Exception(response.statusCode);
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  verifyEmail(String email) async {
    try {
      _apiConfig.url = 'auth/verify-email?random=$email';
      _apiConfig.headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      _apiConfig.body = null;
      final response = await _apiConfig.post();
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception(response.statusCode);
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  resetPassword(String email) async {
    try {
      _apiConfig.url = 'auth/rese-password?email=$email';
      _apiConfig.headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      _apiConfig.body = null;
      final response = await _apiConfig.post();
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception(response.statusCode);
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  updatePassword(UpdatePasswordModel updatePasswordModel) async {
    try {
      _apiConfig.url = 'auth/update-password';
      _apiConfig.headers = {'Content-Type': 'application/json'};
      _apiConfig.body = jsonEncode(updatePasswordModel.toJson());
      final response = await _apiConfig.put();
      if (response.statusCode == 200 || response.statusCode == 201) {
        return response.body;
      } else {
        throw Exception(response.statusCode);
      }
    } catch (e) {
      throw Exception(e);
    }
  }
}
