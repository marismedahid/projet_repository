import 'dart:convert';
import 'package:delivery_user_app/shared/service/abstract_api_config.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user_model.dart';
import '../utils.dart';

class ApiUser {
  AbstractApiConfig apiConfig;
  Utils utils = Utils();

  ApiUser(this.apiConfig);

  updateProfile(UserModel registerModel) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      apiConfig.url = 'user/profile';
      apiConfig.headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await utils.getToken(sharedPreferences)}'
      };
      apiConfig.body = jsonEncode(registerModel.toJson());
      final response = await apiConfig.put();
      if (response.statusCode == 200 || response.statusCode == 201) {
        var jsonData = jsonDecode(response.body);
        return UserModel.fromJson(jsonData);
      } else {
        throw Exception(response.statusCode);
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  getProfile() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      apiConfig.url = 'user/profile';
      apiConfig.headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await utils.getToken(sharedPreferences)}'
      };
      apiConfig.body = null;
      final response = await apiConfig.get();
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        return UserModel.fromJson(jsonData);
      } else {
        throw Exception(response.statusCode);
      }
    } catch (e) {
      throw Exception(e);
    }
  }
}
