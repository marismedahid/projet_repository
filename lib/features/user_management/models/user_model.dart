class UserModel {
  int? id;
  String? username;
  String? email;
  String? nni;
  String? phoneNumber;
  String? firstName;
  String? lastName;
  String? gender;
  String? address;
  bool? enabled;
  String? kcId;
  String? password;
  DateTime? createdAt;
  DateTime? updatedAt;

  UserModel({
    this.id,
    this.username,
    this.email,
    this.nni,
    this.phoneNumber,
    this.firstName,
    this.lastName,
    this.gender,
    this.address,
    this.enabled,
    this.kcId,
    this.password,
    this.createdAt,
    this.updatedAt,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
    nni = json['nni'];
    phoneNumber = json['phoneNumber'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    gender = json['gender'];
    address = json['address'];
    enabled = json['enabled'];
    kcId = json['kcId'];
    password = json['password'];
    // createdAt = DateTime.parse(json["createdAt"]);
    // updatedAt = DateTime.parse(json["updatedAt"]);
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "email": email,
        "nni": nni,
        "phoneNumber": phoneNumber,
        "firstName": firstName,
        "lastName": lastName,
        "gender": gender,
        "address": address,
        "enabled": enabled,
        "kcId": kcId,
        "password": password,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
      };
}
