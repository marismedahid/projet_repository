class UpdatePasswordModel {
  String? code;
  String? password;

  UpdatePasswordModel({this.code, this.password});

  UpdatePasswordModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['password'] = this.password;
    return data;
  }
}
