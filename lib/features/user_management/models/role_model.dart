import 'package:flutter/material.dart';

class RoleModel {
  final String title;
  final String description;
  final Widget image;

  RoleModel(
      {required this.title, required this.description, required this.image});
}
