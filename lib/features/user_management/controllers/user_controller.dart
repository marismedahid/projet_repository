import 'package:delivery_user_app/shared/service/api_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user_model.dart';
import '../services/api_user.dart';
import '../utils.dart';

class UserController extends GetxController {
  TextEditingController addressTextController = TextEditingController();
  TextEditingController firstNameTextController = TextEditingController();
  TextEditingController lastNameTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();
  final genders = ['male', 'female'];
  late DateTime selectedDate = DateTime.now();
  var isLoading = false.obs;
  UserModel currentUser = UserModel();
  late ApiUser apiUser = ApiUser(ApiConfig(client: http.Client()));

  late Utils utils = Utils();

  _setIsLoading(bool value) {
    isLoading.value = value;
  }

  static setFirstLogin(bool value) async {
    final _sharedPreferences = await SharedPreferences.getInstance();
    _sharedPreferences.setBool('firstLogin', value);
  }

  static Future<bool> getFirstLogin() async {
    final _sharedPreferences = await SharedPreferences.getInstance();
    if (_sharedPreferences.containsKey('firstLogin')) {
      var _value = _sharedPreferences.getBool('firstLogin');
      return _value!;
    }
    return false;
  }

  Future<void> updateProfile(
      UserModel registerModel, BuildContext context) async {
    try {
      _setIsLoading(true);
      var result = await apiUser.updateProfile(registerModel);
      currentUser = result;
      _setIsLoading(false);
    } catch (e) {
      _setIsLoading(false);
      utils.getExceptionSnackBar(context, e, 'updatePassword');
    }
  }

  Future<void> getProfile(BuildContext context) async {
    try {
      _setIsLoading(true);
      UserModel result = await apiUser.getProfile();

      currentUser = result;
      print('profiiiile');
      print(currentUser);
      _setIsLoading(false);
    } catch (e) {
      _setIsLoading(false);
      utils.getExceptionSnackBar(context, e, 'getProfile');
    }
  }
}
