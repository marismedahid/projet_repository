import 'dart:convert';

import 'package:delivery_user_app/app_route.dart';
import 'package:delivery_user_app/features/user_management/services/api_auth.dart';
import 'package:delivery_user_app/shared/service/api_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../app_routes.dart';
import '../models/login_model.dart';
import '../models/reason_model.dart';
import '../models/user_model.dart';
import '../models/update_password_model.dart';

import '../widgets/confirmation_popup.dart';
import '../utils.dart';
import 'user_controller.dart';
import 'package:jwt_decode/jwt_decode.dart';

class AuthController extends GetxController {
  TextEditingController nniTextController = TextEditingController();
  TextEditingController phoneTextController = TextEditingController();
  TextEditingController emailTextController = TextEditingController();
  TextEditingController usernameTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();
  TextEditingController confirmPasswordTextController = TextEditingController();
  TextEditingController pinInputController = TextEditingController();
  final loginFormKey = GlobalKey<FormState>();

  late RxBool forgotPasswordByPhone = true.obs;
  var isLoading = false.obs;
  late ApiAuth apiAuth = ApiAuth(ApiConfig(client: http.Client()));
  late Utils utils = Utils();
  UserController userController = Get.put(UserController());
  List<ReasonModel> reasons = [
    ReasonModel(id: '1', body: 'nec dui nunc mattis enim'),
    ReasonModel(id: '2', body: 'nec dui nunc mattis enim'),
    ReasonModel(id: '3', body: 'nec dui nunc mattis enim'),
    ReasonModel(id: '4', body: 'nec dui nunc mattis enim'),
    ReasonModel(id: '5', body: 'other'),
  ];

  _setIsLoading(bool value) {
    isLoading.value = value;
  }

  register(UserModel registerModel, BuildContext context) async {
    try {
      _setIsLoading(true);
      var result = await apiAuth.register(registerModel);
      _setIsLoading(false);

      Get.offAllNamed(UserManagementRoutes.login);

      return result != null;
    } catch (e) {
      _setIsLoading(false);
      utils.getExceptionSnackBar(context, e, 'register');
    }
  }

  login(LoginModel loginModel, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      _setIsLoading(true);
      var response = await apiAuth.login(
        loginModel,
      );
      _setIsLoading(false);
      await utils.setToken(response.token, sharedPreferences);
      userController.currentUser = response.user;

      //if (userController.currentUser = response.user) {}

      if (response != null) {
        if (ApiConfig.loggedOutAfterTokenExpiration) {
          await logoutAfterTokenExpiration(false);
          return true;
        }
      }
      return false;
    } catch (e) {
      _setIsLoading(false);
      print('-----------------------------------------');
      print(e);
      print('-----------------------------------------');
      utils.getExceptionSnackBar(context, e, 'login');
    }
  }

  verifyEmail(String email, BuildContext context) async {
    try {
      _setIsLoading(true);
      var jsonResult = await apiAuth.verifyEmail(email);
      var result = jsonDecode(jsonResult);
      _setIsLoading(false);
      if (result != null) return true;
      return false;
    } catch (e) {
      _setIsLoading(false);
      utils.getExceptionSnackBar(context, e, 'verifyEmail');
    }
  }

  resetPassword(String email, BuildContext context) async {
    try {
      _setIsLoading(true);
      var result = await apiAuth.resetPassword(email);
      _setIsLoading(false);
      if (result != null) return true;
      return false;
    } catch (e) {
      _setIsLoading(false);
      utils.getExceptionSnackBar(context, e, 'resetPasswordWithEmail');
    }
  }

  updatePassword(
      UpdatePasswordModel updatePasswordModel, BuildContext context) async {
    try {
      _setIsLoading(true);
      var result = await apiAuth.updatePassword(updatePasswordModel);
      _setIsLoading(false);
      if (result != null) return true;
      return false;
    } catch (e) {
      _setIsLoading(false);
      utils.getExceptionSnackBar(context, e, 'updatePassword');
    }
  }

  logoutAfterTokenExpiration(bool isCalledInPlashScreen) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var expDate =
        _getTimeUntilExpiration(await utils.getToken(sharedPreferences));
    if (expDate.isNegative) {
      Get.toNamed(UserManagementRoutes.login);
    } else {
      Future.delayed(expDate, () async {
        await confirmationPopup(
            twoOptions: false,
            barrierDismissible: false,
            body: 'sessionExpired'.tr,
            context: Get.context!,
            confirmOnPressed: () {
              Get.offAllNamed(UserManagementRoutes.login);
            });
      });

      Get.offAllNamed(AppRoutes.navigationPage);
      //Get.to(NavigationPage());
    }
  }

  _getTimeUntilExpiration(String token) {
    if (token != '') {
      Map<String, dynamic>? decodedToken = Jwt.parseJwt(token);
      var timestamp = decodedToken['exp'];
      DateTime scheduledTime =
          DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
      Duration timeUntilExecution = scheduledTime.difference(DateTime.now());
      return timeUntilExecution;
    } else {
      return Duration(seconds: -1);
    }
  }

  logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userController.currentUser = UserModel();
    await utils.setToken('', sharedPreferences);
    Get.offAllNamed(UserManagementRoutes.login);
  }
}
