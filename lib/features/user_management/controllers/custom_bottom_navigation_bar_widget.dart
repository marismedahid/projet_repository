import 'package:flutter/material.dart';

import 'package:get/get.dart';

class CustomBottomNavigationBarController extends GetxController {
  var currentIndex = 0.obs;

  List<Widget> items = [
    Center(
      child: Text("Home"),
    ),
    Center(
      child: Text("Messages"),
    ),
    Center(
      child: Text("Commendes"),
    ),
    //ProfileTab(),
  ];

  void updateCurrentIndex(int newValue) {
    currentIndex.value = newValue;
  }
}
