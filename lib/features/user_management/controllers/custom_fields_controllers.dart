import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomTextFormFieldController extends GetxController {
  var showPassword = true.obs;
  var alwaysTrue = true.obs;

  final TextEditingController textEditingController = TextEditingController();

  void toggleShowPassword() {
    showPassword.value = !showPassword.value;
  }
}

class CustomDropdownController extends GetxController {
  var alwaysTrue = true.obs;
  var value = 1.obs;

  void setValue(int newValue) {
    value.value = newValue;
  }
}
