import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/panier_controller.dart';
import 'package:delivery_user_app/features/order/screens/cart_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class ButtonPanier extends StatelessWidget {
  ButtonPanier({super.key});

  final CartController cartController = Get.find();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.to(CartScreen()),
      child: Obx(
        () => Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          padding: EdgeInsets.symmetric(horizontal: horizontalSpacer(context)),
          height: verticalSpacer(context) * 2.2,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: AppConstants.customOrange,
          ),
          child: Row(
            children: [
              // SizedBox(
              //   width: horizontalSpacer(context),
              // ),
              Container(
                height: verticalSpacer(context) * 1.5,
                width: verticalSpacer(context) * 1.5,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: Colors.white,
                  ),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Text(
                    '${cartController.dishets.length}',
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.white),
                  ),
                ),
              ),
              Spacer(),
              Text(
                "voir panier",
                style: GoogleFonts.mulish(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: Colors.white),
              ),
              Spacer(),
              Text(
                "${cartController.calculateTotalPrice()}",
                style: GoogleFonts.inter(
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
