import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/app_routes.dart';
import 'package:delivery_user_app/features/order/controllers/panier_controller.dart';
import 'package:delivery_user_app/features/order/models/restaurant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class RestaurantWidget extends StatelessWidget {
  RestaurantWidget({
    super.key,
    required this.restaurant,
  });
  final Restaurant restaurant;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.put(CartController());
        Get.toNamed(OrderManagementRoutes.restaurantDetails,
            arguments: restaurant);
      },
      child: Padding(
        padding: EdgeInsets.only(bottom: verticalSpacer(context) * 1.5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: verticalSpacer(context),
                  right: horizontalSpacer(context)),
              height: fullHeight(context) * 0.2,
              // width: fullWidth(context) * .9,
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                  color: AppConstants.customOrange,
                  image: DecorationImage(
                      image: AssetImage(
                        restaurant.image,
                      ),
                      fit: BoxFit.fill)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  restaurant.discount != null
                      ? Container(
                          height: verticalSpacer(context) * 2,
                          width: horizontalSpacer(context) * 6,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(15),
                                topRight: Radius.circular(15)),
                            color: Colors.white,
                          ),
                          child: Center(
                            child: Text(
                              "${restaurant.discount}% Discount",
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 13,
                                  color: AppConstants.customOrange),
                            ),
                          ),
                        )
                      : const SizedBox(),
                  Container(
                    height: verticalSpacer(context) * 2,
                    width: verticalSpacer(context) * 2,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    child: Icon(
                      restaurant.isFavorite
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: AppConstants.customOrange,
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: verticalSpacer(context),
            ),
            Row(
              children: [
                Container(
                  height: verticalSpacer(context) * 1.7,
                  width: verticalSpacer(context) * 1.7,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(
                            restaurant.logo,
                          ),
                          fit: BoxFit.fill)),
                ),
                SizedBox(
                  width: horizontalSpacer(context) * .5,
                ),
                Text(
                  restaurant.name,
                  style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      fontSize: 18.63,
                      color: AppConstants.black),
                ),
                Spacer(),
                Icon(
                  restaurant.isFavorite ? Icons.star : Icons.star_border,
                  color: AppConstants.customOrange,
                ),
                Text(
                  restaurant.rating,
                  style: GoogleFonts.inter(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: AppConstants.black),
                ),
                SizedBox(
                  width: horizontalSpacer(context) * .2,
                ),
                Text(
                  "(${restaurant.reviewers}+)",
                  style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: Color(0xFF7E8392)),
                ),
              ],
            ),
            SizedBox(
              height: verticalSpacer(context) * 0.5,
            ),
            Text(
              "Minimum : ${restaurant.minimumToDeliver} MRU",
              style: GoogleFonts.inter(
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: Color(0xFF757575)),
            ),
            SizedBox(
              height: verticalSpacer(context) * 0.5,
            ),
            Row(
              children: [
                Icon(Icons.timer_sharp, color: Color(0xFF5C6BC0)),
                SizedBox(
                  width: horizontalSpacer(context) * 0.2,
                ),
                Text(
                  "${restaurant.minTimeToDeliver}-${restaurant.maxTimeToDeliver} mins",
                  style: GoogleFonts.inter(
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: Color(0xFF5C6BC0)),
                ),
                SizedBox(
                  width: horizontalSpacer(context) * 4,
                ),
                SvgPicture.asset("assets/images/icon_deliver_man.svg"),
                SizedBox(width: horizontalSpacer(context) * .7),
                Text(
                  "${restaurant.deliveryPrice} MRU",
                  style: GoogleFonts.inter(
                      fontWeight: FontWeight.w400,
                      fontSize: 12,
                      color: Color(0xFFEA0000)),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
