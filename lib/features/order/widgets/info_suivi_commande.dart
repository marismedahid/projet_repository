import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/widgets/time_estimated.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class InfoSuiviCommande extends StatelessWidget {
  final String? source;
  final String? code;
  final String? destination;
  final String? namedriver;
  final String? telephonedriver;

  const InfoSuiviCommande(
      {super.key,
      this.source,
      this.destination,
      this.namedriver,
      this.telephonedriver,
      this.code});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(8),
        height: fullHeight(context) * 0.6,
        width: fullWidth(context),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Divider(
              thickness: 3,
              endIndent: fullWidth(context) * 0.35,
              indent: fullWidth(context) * 0.35,
            ),
            SizedBox(
              height: fullHeight(context) * 0.02,
            ),
            Container(
              margin: EdgeInsets.only(left: fullWidth(context) * 0.08),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: AppConstants.orng),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 12, right: 12, top: 5, bottom: 5),
                        child: Text("$code",
                            style: const TextStyle(
                                fontSize: 14, color: Colors.white)),
                      ))
                ],
              ),
            ),
            SizedBox(
              height: fullHeight(context) * 0.02,
            ),
            TimeEstime(),
            const Divider(
              endIndent: 20,
              indent: 20,
            ),
            Container(
              margin: const EdgeInsets.all(15),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'From',
                          style: TextStyle(color: AppConstants.bbg),
                        ),
                        Text(source!)
                      ],
                    ),
                    Column(
                      children: [
                        const Text('to',
                            style: TextStyle(color: AppConstants.bbg)),
                        Text(destination!)
                      ],
                    ),
                  ]),
            ),
            SizedBox(
              height: fullHeight(context) * 0.04,
            ),
            Container(
                margin: EdgeInsets.only(left: fullWidth(context) * 0.2),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Driver details',
                        style: TextStyle(color: AppConstants.grey2)),
                    SizedBox(
                      height: fullHeight(context) * 0.01,
                    ),
                    Row(
                      children: [
                        CircleAvatar(
                          child: Image.asset('assets/images/driver.png'),
                        ),
                        SizedBox(
                          width: fullWidth(context) * 0.02,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(namedriver!),
                            Text('ID: ${telephonedriver}')
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: fullHeight(context) * 0.02,
                    ),
                    Row(
                      children: [
                        Container(
                          width: fullWidth(context) * 0.35,
                          height: fullHeight(context) * 0.06,
                          decoration: BoxDecoration(
                              color: AppConstants.customOrange,
                              borderRadius: BorderRadius.circular(100)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset('assets/images/call.png'),
                              SizedBox(
                                width: fullWidth(context) * 0.02,
                              ),
                              const Text(
                                'Appeler',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontFamily: 'Nunito',
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: fullWidth(context) * 0.02,
                        ),
                        Row(
                          children: [
                            SvgPicture.asset('assets/images/doc.svg'),
                            SizedBox(
                              width: fullWidth(context) * 0.01,
                            ),
                            const Text('Message')
                          ],
                        )
                      ],
                    )
                  ],
                )),
          ],
        ));
  }
}
