import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/panier_controller.dart';
import 'package:delivery_user_app/features/order/controllers/restaurant_detail_controller.dart';
import 'package:delivery_user_app/features/order/models/cart_item.dart';
import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class PopularFoods extends StatelessWidget {
  final RestaurantDetailController _restaurantDetailController =
      RestaurantDetailController();
  final CartController _cartController = Get.find();
  final int restaurantId;

  PopularFoods({super.key, required this.restaurantId});

  @override
  Widget build(BuildContext context) {
    _restaurantDetailController.loadPopularFoods(restaurantId);
    return Obx(() => _restaurantDetailController.isLoading.value
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : GridView.count(
            childAspectRatio: 0.8,
            crossAxisCount: 2,
            children: _restaurantDetailController.popularFoods
                .map((e) => FastFoodWidget(
                    fastFoodModel: e, cartController: _cartController))
                .toList()));
  }
}

class FastFoodWidget extends StatelessWidget {
  const FastFoodWidget({
    super.key,
    required this.fastFoodModel,
    required this.cartController,
  });
  final FastFoodModel fastFoodModel;
  final CartController cartController;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: fullWidth(context) * 0.01),
      // height: fullHeight(context) * 0.8,
      // width: fullWidth(context) * 0.4,
      child: Card(
        elevation: 3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(fullWidth(context) *
                    0.03), // Border radius for the top left corner of the image
                topRight: Radius.circular(fullWidth(context) *
                    0.03), // Border radius for the top right corner of the image
              ),
              child: Image.asset(
                height: fullHeight(context) * 0.14,
                width: fullWidth(context) * 0.5,
                //'assets/images/food.jpg',
                fastFoodModel.image!,
                fit: BoxFit.cover,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                padding: EdgeInsets.only(
                  left: 4,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: fullHeight(context) * 0.01),
                    Text(
                      fastFoodModel.name!,
                      style: GoogleFonts.inter(
                        fontSize: 11,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: fullHeight(context) * 0.01),
                    SizedBox(
                      height: fullHeight(context) * 0.001,
                      child: RatingBar.builder(
                        initialRating: 3,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemSize: 15,
                        itemCount: fastFoodModel.rating!,
                        itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) {
                          print(rating);
                        },
                      ),
                    ),
                    SizedBox(
                      height: fullHeight(context) * 0.02,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: AppConstants.roza),
                      width: fullWidth(context) * 0.1,
                      height: fullHeight(context) * 0.03,
                      child: Padding(
                        padding: EdgeInsets.all(
                          fullWidth(context) * 0.01,
                        ),
                        child: Text(
                          //  '10%',
                          '-${fastFoodModel.discount}%',
                          style:
                              TextStyle(fontSize: 10, color: AppConstants.orn),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: fullHeight(context) * 0.008,
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                            left: fullWidth(context) * 0.01,
                          ),
                          child: Text(fastFoodModel.price!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 10)),
                        ),
                        SizedBox(width: fullWidth(context) * 0.25),
                        InkWell(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: const Text("Confirmer l'ajout"),
                                  content: const Text(
                                      "Êtes-vous sûr de vouloir ajouter cet article au panier ?"),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text("Annuler"),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        print('item panier');
                                        print(cartController.dishets);
                                        // Créer un nouvel article
                                        FastFoodModel newItem = FastFoodModel(
                                          id: fastFoodModel.id,
                                          quantity: '01',
                                          price: fastFoodModel.price,
                                          note: "Note lorem lorem lorem ",
                                          name: fastFoodModel.name,
                                          image: fastFoodModel.image!,
                                        );
                                        // Ajouter l'article au panier en utilisant le contrôleur
                                        cartController.addToCart(newItem);
                                        Navigator.of(context).pop();
                                      },
                                      child: Text("Confirmer"),
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                          child: CircleAvatar(
                            radius: fullWidth(context) * 0.04,
                            backgroundColor: AppConstants.orn,
                            child: Icon(Icons.add, color: Colors.white),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
