import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/traking_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TimeEstime extends StatelessWidget {
  TimeEstime({super.key});
  final TrakingController traking = TrakingController();

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: SizedBox(
        height: fullHeight(context) * 0.1,
        child: Obx(
          () => SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: fullWidth(context) * 0.08),
                  child: Column(
                    children: [
                      Text(
                        'Temps restant'.tr,
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Nunito',
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                            '${traking.remainingTime.toString()}'
                            ' mm',
                            style: TextStyle(color: AppConstants.grey1)),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  width: fullWidth(context) * 0.08,
                ),
                VerticalDivider(thickness: 1, color: Color(0xff7C7B7A)),
                SizedBox(
                  width: fullWidth(context) * 0.08,
                ),
                Column(
                  children: [
                    Text(
                      'Temps estimé'.tr,
                      style: const TextStyle(
                        fontSize: 16,
                        fontFamily: 'Nunito',
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        '${traking.time.value.toString()} mm',
                        style: const TextStyle(
                            fontSize: 16, color: AppConstants.grey1),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
