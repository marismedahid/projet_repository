import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/panier_controller.dart';
import 'package:delivery_user_app/features/order/controllers/restaurant_detail_controller.dart';
import 'package:delivery_user_app/features/order/models/cart_item.dart';
import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Pizza extends StatelessWidget {
  Pizza({super.key, required this.menuId});
  final RestaurantDetailController _restaurantDetailController =
      RestaurantDetailController();
  final String menuId;
  final CartController _cartController = Get.find();

  @override
  Widget build(BuildContext context) {
    _restaurantDetailController.loadFoods(menuId);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            child: Obx(
          () => _restaurantDetailController.isLoading.value
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  itemCount: _restaurantDetailController
                      .foods.length, // Nombre total d'éléments dans la liste
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: horizontalSpacer(context) * .4,
                          vertical: 8.0),
                      child: PizzaWidget(
                        cartController: _cartController,
                        fastFoodModel: _restaurantDetailController.foods[index],
                      ),
                    );
                  },
                ),
        )),
      ],
    );
  }
}

class PizzaWidget extends StatelessWidget {
  const PizzaWidget({
    super.key,
    required this.fastFoodModel,
    required this.cartController,
  });

  final FastFoodModel fastFoodModel;

  final CartController cartController;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 3,
      borderRadius: BorderRadius.circular(10),
      child: Row(
        children: [
          Container(
            height: verticalSpacer(context) * 3,
            width: horizontalSpacer(context) * 3,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(
                      fastFoodModel.image!,
                    ))),
          ),
          SizedBox(
            width: fullWidth(context) * 0.035,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(fastFoodModel.name!,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, color: AppConstants.bold)),
              Text(fastFoodModel.description!,
                  style: const TextStyle(fontSize: 10)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(fastFoodModel.price!,
                        style: const TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(
                    width: fullWidth(context) * 0.54,
                  ),
                  CircleAvatar(
                    radius: fullWidth(context) * 0.03,
                    backgroundColor: Colors.orange,
                    child: GestureDetector(
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: const Text("Confirmer l'ajout"),
                                content: const Text(
                                    "Êtes-vous sûr de vouloir ajouter cet article au panier ?"),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text("Annuler"),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      print('item panier');
                                      print(cartController.dishets);
                                      // Créer un nouvel article
                                      FastFoodModel newItem = FastFoodModel(
                                        id: fastFoodModel.id,
                                        quantity: fastFoodModel.quantity,
                                        price: fastFoodModel.price,
                                        note: "Note lorem lorem lorem ",
                                        name: fastFoodModel.name,
                                        image: fastFoodModel.image!,
                                      );
                                      // Ajouter l'article au panier en utilisant le contrôleur
                                      cartController.addToCart(newItem);
                                      Navigator.of(context).pop();
                                    },
                                    child: Text("Confirmer"),
                                  ),
                                ],
                              );
                            },
                          );
                        },
                        child: Icon(Icons.add, color: Colors.white)),
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
