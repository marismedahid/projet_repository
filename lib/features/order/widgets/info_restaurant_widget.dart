import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class InfoRestaurant extends StatelessWidget {
  String? image;
  bool? isFavorite;

  final String? rating;
  final String? reviewers;
  final String? minimumToDeliver;
  final String? minTimeToDeliver;
  final String? maxTimeToDeliver;
  final String? deliveryPrice;
  InfoRestaurant(
      {super.key,
      this.image,
      this.isFavorite,
      this.rating,
      this.reviewers,
      this.minTimeToDeliver,
      this.minimumToDeliver,
      this.maxTimeToDeliver,
      this.deliveryPrice});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: fullHeight(context) * 0.24,
          width: fullWidth(context),
          child: Image.asset(
            fit: BoxFit.cover,
            image!,
          ),
        ),
        SizedBox(
          height: fullHeight(context) * 0.1,
        ),
        Container(
            margin: EdgeInsets.only(
              left: fullWidth(context) * 0.08,
            ),
            child: Row(
              children: [
                Icon(
                  Icons.motorcycle_sharp,
                  color: AppConstants.orn,
                  size: 14,
                ),
                SizedBox(
                  width: fullWidth(context) * 0.02,
                ),
                Text(
                  'Livraison Gratuit',
                  style: GoogleFonts.inter(
                    color: AppConstants.orn,
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            )),
        SizedBox(
          height: fullHeight(context) * 0.013,
        ),
        Container(
          margin: EdgeInsets.only(
            left: fullWidth(context) * 0.08,
          ),
          child: Row(
            children: [
              Icon(
                Icons.access_time_sharp,
                size: 15,
              ),
              SizedBox(
                width: fullWidth(context) * 0.02,
              ),
              Text('${minTimeToDeliver}-${maxTimeToDeliver} min',
                  style: GoogleFonts.inter(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                  )),
              SizedBox(
                width: fullWidth(context) * 0.08,
              ),
              Text('Minimum : ${deliveryPrice} MRU',
                  style: GoogleFonts.inter(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                  ))
            ],
          ),
        ),
        SizedBox(
          height: fullHeight(context) * 0.013,
        ),
        Container(
          margin: EdgeInsets.only(
            left: fullWidth(context) * 0.08,
          ),
          child: Row(
            children: [
              Image.asset('assets/images/etoil.png'),
              SizedBox(width: fullWidth(context) * 0.02),
              SizedBox(
                  width: fullWidth(context) * 0.3,
                  child: Text(
                    '${rating}' ' étoiles (${reviewers}+)',
                    style: GoogleFonts.inter(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                    ),
                  )),
              SizedBox(width: fullWidth(context) * 0.5),
              Icon(
                Icons.arrow_forward_ios,
                size: 14,
              ),
            ],
          ),
        ),
        SizedBox(
          height: fullHeight(context) * 0.013,
        ),
        Container(
          margin: EdgeInsets.only(
            left: fullWidth(context) * 0.08,
          ),
          child: Row(
            children: [
              Image.asset('assets/images/iconpromo.png'),
              SizedBox(width: fullWidth(context) * 0.02),
              Text(
                'Promo Disponible',
                style: GoogleFonts.inter(
                    fontSize: 11,
                    fontWeight: FontWeight.w400,
                    color: AppConstants.black1),
              ),
              SizedBox(width: fullWidth(context) * 0.55),
              const Icon(
                Icons.arrow_forward_ios,
                size: 14,
              ),
            ],
          ),
        ),
        SizedBox(
          height: fullHeight(context) * 0.01,
        ),
        Container(
          margin: EdgeInsets.only(
            left: fullWidth(context) * 0.08,
          ),
          child: Row(
            children: [
              Image.asset('assets/images/address.png'),
              SizedBox(width: fullWidth(context) * 0.01),
              Text(
                '424C+JFR, Unnamed Road, Nouakchott, Mauritanie',
                style: GoogleFonts.inter(
                  fontSize: 11,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(width: fullWidth(context) * 0.058),
              Icon(
                Icons.arrow_forward_ios,
                size: 14,
              ),
            ],
          ),
        )
      ],
    );
  }
}
