import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class CartItemWidget extends StatelessWidget {
  final FastFoodModel dishet;
  final ItemWidgetController _itemWidgetController = ItemWidgetController();

  CartItemWidget({
    super.key,
    required this.dishet,
  });

  @override
  Widget build(BuildContext context) {
    //   _itemWidgetController.setQuantity(int.parse(dishet.quantity!));
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      width: fullWidth(context) * .9,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: verticalSpacer(context) * 3.5,
            width: verticalSpacer(context) * 3.5,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: AppConstants.customOrange,
                image: DecorationImage(
                    image: AssetImage(
                      dishet.image!,
                    ),
                    fit: BoxFit.fill)),
          ),
          SizedBox(
            width: horizontalSpacer(context) * .5,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    dishet.name!,
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        color: AppConstants.black),
                  ),
                ],
              ),
              Text(
                dishet.note!,
                style: GoogleFonts.inter(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: Color(0xFF8C8A9D)),
              ),
              SizedBox(
                width: fullWidth(context) * .7,
                height: verticalSpacer(context),
                child: Row(
                  children: [
                    Text(
                      "${dishet.price} MRU ",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: Color(0xFFFF6600)),
                    ),
                    Spacer(),
                    GestureDetector(
                      onTap: () {
                        _itemWidgetController.decrementQuantity();
                        dishet.quantity =
                            _itemWidgetController.quantity.value.toString();
                      },
                      child: Container(
                        height: verticalSpacer(context),
                        width: verticalSpacer(context),
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 1, color: AppConstants.customOrange),
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        child: Center(
                          child: Text(
                            "-",
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w600,
                                fontSize: 14,
                                color: AppConstants.customOrange),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: horizontalSpacer(context) * .5,
                    ),
                    Obx(
                      () => Text(
                        '${_itemWidgetController.quantity.value}',
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: AppConstants.black),
                      ),
                    ),
                    SizedBox(
                      width: horizontalSpacer(context) * .5,
                    ),
                    GestureDetector(
                      onTap: () {
                        _itemWidgetController.incrementQuantity();
                        dishet.quantity =
                            _itemWidgetController.quantity.value.toString();
                      },
                      child: Container(
                        height: verticalSpacer(context),
                        width: verticalSpacer(context),
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppConstants.customOrange,
                        ),
                        child: Center(
                          child: Text(
                            "+",
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w600,
                                fontSize: 14,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class ItemWidgetController extends GetxController {
  var quantity = 1.obs;

  void setQuantity(newValue) {
    quantity.value = newValue;
  }

  void incrementQuantity() {
    quantity.value++;
  }

  void decrementQuantity() {
    if (quantity.value > 0) {
      quantity.value--;
    }
  }
}
