import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/models/cart_item.dart';
import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RecommendedItemWidget extends StatelessWidget {
  RecommendedItemWidget({
    super.key,
    required this.dishet,
  });

  final FastFoodModel dishet;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: fullWidth(context) * .5,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: fullHeight(context) * 0.2,
            width: fullWidth(context) * .5,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
                color: AppConstants.customOrange,
                image: DecorationImage(
                    image: AssetImage(
                      dishet.image!,
                    ),
                    fit: BoxFit.fill)),
          ),
          SizedBox(
            height: verticalSpacer(context) * .5,
          ),
          Text(
            dishet.name!,
            style: GoogleFonts.inter(
                fontWeight: FontWeight.w600,
                fontSize: 11,
                color: Color(0xFF363842)),
          ),
          SizedBox(
            height: verticalSpacer(context) * .5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "${dishet.price} MRU ",
                style: GoogleFonts.inter(
                    fontWeight: FontWeight.w600,
                    fontSize: 14.36,
                    color: Color(0xFF363842)),
              ),
              Container(
                height: verticalSpacer(context) * 1.5,
                width: verticalSpacer(context) * 1.5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: AppConstants.customOrange,
                ),
                child: const Icon(
                  Icons.add,
                  size: 18,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
