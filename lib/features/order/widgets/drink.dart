import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/restaurant_detail_controller.dart';
import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Drink extends StatelessWidget {
  Drink({super.key});
  final RestaurantDetailController _restaurantDetailController =
      RestaurantDetailController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            margin: EdgeInsets.only(
              left: fullWidth(context) * 0.058,
            ),
            child: const Text('Boissons',
                style: TextStyle(fontWeight: FontWeight.bold))),
        SizedBox(
          height: fullHeight(context) * 0.02,
        ),
        Expanded(
          child: ListView.builder(
              itemCount: _restaurantDetailController.drinks.length,
              itemBuilder: (context, index) {
                return DrinkWidget(
                  fastFoodModel: _restaurantDetailController.drinks[index],
                );
              }),
        )
      ],
    );
  }
}

class DrinkWidget extends StatelessWidget {
  final FastFoodModel fastFoodModel;
  const DrinkWidget({
    super.key,
    required this.fastFoodModel,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: fullWidth(context),
      height: fullHeight(context) * 0.13,
      margin: EdgeInsets.only(
          left: fullWidth(context) * 0.058,
          //right: fullWidth(context) * 0.01,
          top: fullWidth(context) * 0.01),
      child: Card(
        elevation: 3,
        //borderRadius: BorderRadius.circular(10),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                bottomLeft: Radius.circular(10),
              ),
              child: Image.asset(
                height: fullHeight(context) * 0.13,
                fastFoodModel.image!,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: fullWidth(context) * 0.035,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(fastFoodModel.name!),
                Text(fastFoodModel.price!, style: TextStyle(fontSize: 10)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Text('100',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      width: fullWidth(context) * 0.4,
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: fullHeight(context) * 0.04,
                        ),
                        const CircleAvatar(
                          radius: 13,
                          backgroundColor: AppConstants.orn,
                          child: Icon(Icons.add, color: Colors.white),
                        ),
                      ],
                    )
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
