import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ItemWidget extends StatelessWidget {
  const ItemWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          top: fullHeight(context) * 0.02, left: fullWidth(context) * 0.035),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CircleAvatar(
            backgroundColor: Colors.white,
            child: IconButton(
                onPressed: () => Get.back(),
                icon: Icon(Icons.arrow_back_ios_new)),
          ),
          SizedBox(
            width: fullWidth(context) * 0.65,
          ),
          Card(
            elevation: 3, // Adjust the elevation (shadow) as needed
            shape: RoundedRectangleBorder(
              // Optional: Defines the shape of the card
              borderRadius: BorderRadius.circular(
                  16.0), // Adjust the border radius as needed
            ),
            color:
                Colors.white, // Optional: Set the background color of the card
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                      16.0), // Adjust the border radius as needed

                  color: Colors.white, // Background color of the container
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1), // Shadow color
                      spreadRadius: 5, // Spread radius
                      blurRadius: 7, // Blur radius
                      offset: Offset(0, 3), // Offset from the container
                    ),
                  ],
                ),
                padding: EdgeInsets.all(fullHeight(context) * 0.015),
                child: Icon(
                  Icons.search,
                  color: AppConstants.orngopa,
                )),
          ),
        ],
      ),
    );
  }
}
