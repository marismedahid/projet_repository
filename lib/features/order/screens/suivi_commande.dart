import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/traking_controller.dart';
import 'package:delivery_user_app/features/order/screens/cart_screen.dart';
import 'package:delivery_user_app/features/user_management/widgets/large_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

class SuiviCommande extends StatelessWidget {
  SuiviCommande({super.key});
  final TrakingController controller = Get.put(TrakingController());

  final LatLng depart = LatLng(37.4219999, -122.0840575);
  final LatLng arrive = LatLng(37.7749, -122.4194);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        SizedBox(
          height: fullHeight(context) * 0.045,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: PageTitle(title: "Traking"),
        ),
        Expanded(
          child: Stack(
            children: [
              Obx(
                () => GoogleMap(
                  initialCameraPosition: controller.cameraPosition.value,
                  onCameraMove: (CameraPosition newPosition) {
                    controller.updateCameraPosition(newPosition);
                  },
                  markers: {
                    Marker(
                      markerId: MarkerId('source'.tr),
                      position: depart,
                      icon: BitmapDescriptor.defaultMarker,
                      infoWindow:
                          InfoWindow(title: 'Position de la commande'.tr),
                    ),
                    Marker(
                      markerId: MarkerId('destinationLocation'.tr),
                      position: arrive,
                      icon: BitmapDescriptor.defaultMarker,
                      infoWindow: InfoWindow(title: 'DES'),
                    ),
                  },
                  polylines: {
                    Polyline(
                      polylineId: PolylineId('route'.tr),
                      color: Colors.red,
                      width: 40,
                      points: controller.polylineCoordinates,
                    ),
                  },
                  onTap: (LatLng latLng) {
                    controller.drawRoute(depart, arrive);
                  },
                ),
              )
            ],
          ),
        ),
        Container(
          width: fullWidth(context) * 0.5,
          child: LargeButton(
            onPressed: () {
              controller.showMyBottomSheet();
            },
            label: 'voir status commande',
          ),
        ),
      ],
    ));
  }
}
