import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/detail_controller.dart';
import 'package:delivery_user_app/features/order/controllers/panier_controller.dart';
import 'package:delivery_user_app/features/order/controllers/restaurant_detail_controller.dart';
import 'package:delivery_user_app/features/order/models/restaurant.dart';
import 'package:delivery_user_app/features/order/screens/name_restaurant.dart';
import 'package:delivery_user_app/features/order/screens/row_item_widget.dart';
import 'package:delivery_user_app/features/order/widgets/button_panier_widget.dart';
import 'package:delivery_user_app/features/order/widgets/info_restaurant_widget.dart';
import 'package:delivery_user_app/shared/widgets/loading_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailsScreen extends StatelessWidget {
  DetailsScreen({super.key});
  final DetailsController controller = Get.put(DetailsController());
  final RestaurantDetailController restaurantDetailController =
      RestaurantDetailController();

  final Restaurant restaurant = Get.arguments;
  final CartController cartController = Get.find();

  @override
  Widget build(BuildContext context) {
    controller.loadMenus(restaurant.id);
    restaurantDetailController.loadPopularFoods(restaurant.id);
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              InfoRestaurant(
                image: restaurant.image,
                minTimeToDeliver: restaurant.minTimeToDeliver,
                maxTimeToDeliver: restaurant.maxTimeToDeliver,
                rating: restaurant.rating,
                reviewers: restaurant.reviewers,
                deliveryPrice: restaurant.deliveryPrice,
              ),
              const Positioned(top: 30, child: ItemWidget()),
              Positioned(
                  top: 170,
                  child: NameRestaurant(
                    logo: restaurant.logo,
                    name: restaurant.name,
                    timeOuvert: '8',
                    timeFerme: '12',
                  ))
            ],
          ),
          SizedBox(
            height: fullHeight(context) * 0.05,
          ),
          Obx(
            () => controller.isLoading.value
                ? const LoadingButtonWidget()
                : SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: controller.menus.map((menu) {
                        return GestureDetector(
                          onTap: () => controller
                              .setCurrentIndex(controller.menus.indexOf(menu)),
                          child: Container(
                            margin: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: controller.currentIndex.value ==
                                        controller.menus.indexOf(menu)
                                    ? Colors.transparent
                                    : Colors.black
                                        .withOpacity(0.1), // Border color
                                // Border width
                              ),
                              borderRadius: BorderRadius.circular(8),
                              color: controller.currentIndex.value ==
                                      controller.menus.indexOf(menu)
                                  ? AppConstants.orn
                                  : Colors.white,
                            ),
                            width: fullWidth(context) * 0.3,
                            height: fullHeight(context) * 0.04,
                            child: Center(
                                child: Text(
                              menu.name!,
                              style: TextStyle(
                                  color: controller.currentIndex.value ==
                                          controller.menus.indexOf(menu)
                                      ? Colors.white
                                      : Colors.black),
                            )),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
          ),
          // SizedBox(height: 20),
          Obx(() => controller.isLoading.value
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : controller.menus.isEmpty
                  ? const Center(
                      child: Text("Pas de menus"),
                    )
                  : SizedBox(
                      width: fullWidth(context),
                      height: fullHeight(context) * 0.32,
                      child: controller.getContent(
                          controller.menus[controller.currentIndex.value].id!),
                    )),

          Obx(() =>
              cartController.dishets.isNotEmpty ? ButtonPanier() : SizedBox()),
          // CartItemWidget(cartItem: cartItem):SizedBox()
        ],
      ),
      //floatingActionButton
    );
  }
}
