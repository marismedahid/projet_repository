import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/order_controller.dart';
import 'package:delivery_user_app/features/order/controllers/panier_controller.dart';
import 'package:delivery_user_app/features/order/models/cart_item.dart';
import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:delivery_user_app/features/order/screens/suivi_commande.dart';
import 'package:delivery_user_app/features/order/widgets/cart_item_widget.dart';
import 'package:delivery_user_app/features/order/widgets/recommended_item_widget.dart';
import 'package:delivery_user_app/features/user_management/widgets/leading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class CartScreen extends StatelessWidget {
  CartScreen({super.key});

  final CartController cartController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => Container(
          padding: EdgeInsets.only(
              top: verticalSpacer(context) * 2.5,
              bottom: verticalSpacer(context) * .5,
              left: horizontalSpacer(context),
              right: horizontalSpacer(context)),
          height: fullHeight(context),
          width: fullWidth(context),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const PageTitle(title: "Détails du Commande"),
              SizedBox(
                height: verticalSpacer(context) * .5,
              ),
              SizedBox(
                height: cartController.dishets.length < 3
                    ? 5 *
                        verticalSpacer(context) *
                        cartController.dishets.length
                    : 3.25 * verticalSpacer(context) * 4,
                child: ListView.builder(
                  itemCount: cartController.dishets.length,
                  itemBuilder: (context, index) {
                    return CartItemWidget(
                      dishet: cartController.dishets[index],
                    );
                    //cartItems[index];
                  },
                ),
              ),
              SizedBox(
                height: verticalSpacer(context),
              ),
              Row(
                children: [
                  const Icon(
                    Icons.add,
                    color: AppConstants.customOrange,
                  ),
                  SizedBox(
                    width: horizontalSpacer(context) * .5,
                  ),
                  Text(
                    "Ajouter plus",
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: AppConstants.customOrange),
                  ),
                ],
              ),
              SizedBox(
                height: verticalSpacer(context),
              ),
              Text(
                "Populaire avec vos commandes",
                style: GoogleFonts.mulish(
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: Colors.black),
              ),
              SizedBox(
                height: verticalSpacer(context),
              ),
              RecommendedItemWidget(
                dishet: cartController.dishets[0],
              ),
              SizedBox(
                height: verticalSpacer(context) * .5,
              ),
              CreateOrderButton(cartController: cartController)
            ],
          ),
        ),
      ),
    );
  }
}

class CreateOrderButton extends StatelessWidget {
  CreateOrderButton({
    super.key,
    required this.cartController,
  });

  final CartController cartController;
  final OrderController _orderController = OrderController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _orderController.createOrder();
      },
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: horizontalSpacer(context)),
          height: verticalSpacer(context) * 2.5,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: AppConstants.customOrange,
          ),
          child: Obx(
            () => _orderController.isLoading.value
                ? Lodingimage()
                : Row(
                    children: [
                      // SizedBox(
                      //   width: horizontalSpacer(context),
                      // ),
                      Container(
                        height: verticalSpacer(context) * 1.5,
                        width: verticalSpacer(context) * 1.5,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: Colors.white,
                          ),
                          shape: BoxShape.circle,
                        ),
                        child: Center(
                          child: Text(
                            "${cartController.dishets.length}",
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Colors.white),
                          ),
                        ),
                      ),
                      Spacer(),
                      GestureDetector(
                        onTap: () => Get.to(SuiviCommande()),
                        child: Text(
                          "Commender",
                          style: GoogleFonts.mulish(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              color: Colors.white),
                        ),
                      ),
                      Spacer(),
                      Text(
                        "${cartController.calculateTotalPrice()}",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.white),
                      ),
                    ],
                  ),
          )),
    );
  }
}

// List cartItems = [
//   CartItemWidget(
//     dishet: FastFoodModel(
//       quantity: "01",
//       price: "120",
//       note: "Note lorem lorem lorem ",
//       name: "Spaghetti bolognese",
//       image: "assets/images/image_02.png",
//     ),
//   ),
//   CartItemWidget(
//    dishet: FastFoodModel(
//       quantity: "01",
//       price: "120",
//       note: "Note lorem lorem lorem ",
//       name: "Spaghetti bolognese",
//       image: "assets/images/image_02.png",
//     ),
//   ),
//   CartItemWidget(
//     dishet: FastFoodModel(
//       quantity: "01",
//       price: "120",
//       note: "Note lorem lorem lorem ",
//       name: "Spaghetti bolognese",
//       image: "assets/images/image_02.png",
//     ),
//   )
// ];

class PageTitle extends StatelessWidget {
  const PageTitle({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircularButton(
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            size: calculateSize(context, 0.02),
            weight: 50,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        Spacer(),
        Text(
          title,
          style: GoogleFonts.inter(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: AppConstants.black),
        ),
        Spacer(),
      ],
    );
  }
}

class CircularButton extends StatelessWidget {
  const CircularButton({
    super.key,
    required this.icon,
    required this.onPressed,
  });
  final Icon icon;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed();
      },
      child: Container(
        padding: EdgeInsets.all(calculateSize(context, 0.01)),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 1, color: Color(0xFFEDEDED))),
        child: icon,
      ),
    );
  }
}
