import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:delivery_user_app/features/order/controllers/homepage_controller.dart';
import 'package:delivery_user_app/features/order/models/restaurant.dart';
import 'package:delivery_user_app/features/order/widgets/restaurant_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class Homepage extends StatelessWidget {
  Homepage({super.key});
  final HomepageController _homepageController = HomepageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(
            right: fullWidth(context) * 0.03,
            left: fullWidth(context) * 0.03,
            top: fullWidth(context) * 0.15,
            bottom: fullWidth(context) * 0.1),
        height: fullHeight(context),
        width: fullWidth(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                const Icon(
                  Icons.location_on,
                  color: AppConstants.customOrange,
                  size: 24,
                ),
                SizedBox(
                  width: horizontalSpacer(context),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Rue ahmed dadah TVZ",
                      style: GoogleFonts.roboto(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: const Color(0xFF0D0D0D)),
                    ),
                    Text(
                      "Nouakchott",
                      style: GoogleFonts.roboto(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: const Color(0xFF6C757D)),
                    )
                  ],
                ),
                const Spacer(),
                SvgPicture.asset("assets/svg/icon_notification.svg"),
                SizedBox(
                  width: horizontalSpacer(context),
                ),
                SvgPicture.asset("assets/svg/icon_store.svg")
              ],
            ),
            Expanded(
              child: ListView(
                children: [
                  SizedBox(
                    height: verticalSpacer(context) * 1.5,
                  ),
                  Text(
                    '${'Bienvenue Ahmed'.toUpperCase()} 👋',
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 15.26,
                        color: const Color(0xFF666C89)),
                  ),
                  SizedBox(
                    height: verticalSpacer(context) * 1.5,
                  ),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                    height: verticalSpacer(context) * 3,
                    decoration: BoxDecoration(
                        color: const Color(0xFFFBFBFB),
                        border: Border.all(
                            width: 2, color: const Color(0xFFF2F2F2)),
                        borderRadius: BorderRadius.circular(10.9)),
                    child: Row(
                      children: [
                        Text(
                          "Vous voulez rechercher?",
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w600,
                              fontSize: 14.17,
                              color: const Color(0xFF9B9E9F)),
                        ),
                        const Spacer(),
                        Container(
                          height: verticalSpacer(context) * 1.5,
                          width: verticalSpacer(context) * 1.5,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: AppConstants.customOrange),
                          child: Center(
                              child: SvgPicture.asset(
                                  "assets/svg/icon_search.svg")),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: verticalSpacer(context) * 1.2,
                  ),
                  SizedBox(
                    height: fullHeight(context) * 0.25,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: items.length,
                      itemBuilder: (context, index) {
                        return items[index];
                      },
                    ),
                  ),
                  SizedBox(
                    height: verticalSpacer(context) * 1.2,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 30,
                        width: 4,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppConstants.customOrange),
                      ),
                      SizedBox(
                        width: horizontalSpacer(context),
                      ),
                      Text(
                        "Meilleurs restaurants",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w600,
                            fontSize: 19.67,
                            color: const Color(0xFF1A1D1F)),
                      ),
                      const Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Voir tout",
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w500,
                                fontSize: 13,
                                color: AppConstants.customOrange),
                          ),
                          SizedBox(
                            width: horizontalSpacer(context) * 0.5,
                          ),
                          const Icon(
                            Icons.arrow_forward_ios,
                            color: AppConstants.customOrange,
                            size: 12,
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: verticalSpacer(context),
                  ),

                  Obx(() => _homepageController.isLoadingRestaurants.value
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : Column(
                          children: _homepageController.restaurants
                              .map((restaurant) =>
                                  RestaurantWidget(restaurant: restaurant))
                              .toList(),
                        ))

                  // RestaurantWidget(
                  //     restaurant: Restaurant(
                  //   image: "assets/images/image_02.png",
                  //   isFavorite: true,
                  //   logo: "assets/images/logo_01.png",
                  //   name: "Timeless",
                  //   rating: "4.0",
                  //   reviewers: "1000",
                  //   minimumToDeliver: "1000",
                  //   minTimeToDeliver: "15",
                  //   maxTimeToDeliver: "60",
                  //   deliveryPrice: "150",
                  // )),
                  // RestaurantWidget(
                  //   restaurant: Restaurant(
                  //     image: "assets/images/image_02.png",
                  //     isFavorite: false,
                  //     discount: "10",
                  //     logo: "assets/images/logo_01.png",
                  //     name: "Palacio",
                  //     rating: "5.1",
                  //     reviewers: "1200",
                  //     minimumToDeliver: "5000",
                  //     minTimeToDeliver: "15",
                  //     maxTimeToDeliver: "60",
                  //     deliveryPrice: "150",
                  //   ),
                  // )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

List items = [
  const PromoItem(
    cardColor: Color(0xFFF25E23),
  ),
  const PromoItem(
    cardColor: Color(0xFF83C1DE),
  ),
  const PromoItem(
    cardColor: Color(0xFFF25E23),
  ),
];

class PromoItem extends StatelessWidget {
  const PromoItem({
    super.key,
    required this.cardColor,
  });
  final Color cardColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 10),
      padding: EdgeInsets.symmetric(
          vertical: verticalSpacer(context) * .8,
          horizontal: verticalSpacer(context)),
      height: fullHeight(context) * 0.25,
      width: fullWidth(context) * .8,
      decoration: BoxDecoration(
          color: cardColor.withOpacity(0.12),
          borderRadius: BorderRadius.circular(10.9)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                "Offer Ramadan Service",
                style: GoogleFonts.inter(
                    fontWeight: FontWeight.w600,
                    fontSize: 13,
                    color: const Color(0xFF33383F)),
              ),
              SizedBox(
                width: horizontalSpacer(context) * .5,
              ),
              SvgPicture.asset("assets/svg/icon_black_exclamation.svg")
            ],
          ),
          SizedBox(
            height: verticalSpacer(context),
          ),
          Text(
            "Obtenez -25%",
            style: GoogleFonts.inter(
                fontWeight: FontWeight.w600,
                fontSize: 36,
                color: const Color(0xFF1A1D1F)),
          ),
          SizedBox(
            height: verticalSpacer(context),
          ),
          Container(
            width: fullWidth(context) * .45,
            height: verticalSpacer(context) * 2.5,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100), color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Profitez de l'offre",
                  style: GoogleFonts.inter(
                      fontWeight: FontWeight.w500,
                      fontSize: 13.1,
                      color: AppConstants.customOrange),
                ),
                SizedBox(
                  width: horizontalSpacer(context) * 0.8,
                ),
                const Icon(
                  Icons.arrow_forward_ios,
                  color: AppConstants.customOrange,
                  size: 16,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
