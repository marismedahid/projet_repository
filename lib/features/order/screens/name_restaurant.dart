import 'package:delivery_user_app/app_config/app_constant.dart';
import 'package:delivery_user_app/app_config/dimension_util.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NameRestaurant extends StatelessWidget {
  final String? name;
  final String? timeOuvert;
  final String? timeFerme;
  final String? logo;
  const NameRestaurant(
      {super.key, this.logo, this.name, this.timeFerme, this.timeOuvert});

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Container(
        padding: const EdgeInsets.all(2), // Adjust padding as needed
        decoration: BoxDecoration(
          color: Colors.white, // Border color
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.white, // Border color
            width: fullWidth(context) * 0.005, // Border width
          ),
        ),
        margin: EdgeInsets.only(
          left: fullWidth(context) * 0.08,
        ),
        child: CircleAvatar(
            radius: fullHeight(context) * 0.054,
            child: Image.asset(
              logo!,
            )),
      ),
      SizedBox(
        width: fullWidth(context) * 0.04,
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: fullHeight(context) * 0.045,
          ),
          Text(name!,
              style: GoogleFonts.inter(
                fontSize: 24,
                fontWeight: FontWeight.w600,
              )),
          Padding(
            padding: EdgeInsets.only(
              bottom: fullHeight(context) * 0.01,
            ),
            child: Text(
              '${timeOuvert!}-${timeFerme} h',
              style: GoogleFonts.inter(
                fontSize: 14,
                color: AppConstants.orn,
                fontWeight: FontWeight.w500,
              ),
            ),
          )
        ],
      )
    ]);
  }
}
