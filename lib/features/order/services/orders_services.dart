import 'dart:convert';
import 'dart:developer';

import 'package:delivery_user_app/app_config/http_gateway.dart';
import 'package:delivery_user_app/features/order/controllers/panier_controller.dart';
import 'package:delivery_user_app/features/order/models/order.dart';
import 'package:delivery_user_app/shared/service/base_url.dart';
import 'package:get/get.dart';

class OrderServices {
  HttpGateway httpGateway = HttpGateway();
  final CartController _cartController = Get.find();

  createOrder() async {
    print("${BaseUrls.apiCommandeManagement}/dorders");
    final httpResponse = await httpGateway.post(
        "${BaseUrls.apiCommandeManagement}/dorders",
        json.encode(Order(
                startDate: DateTime.now(),
                endDate: DateTime.now().add(const Duration(hours: 2)))
            .toJson()));

    print('------------------------------------');
    print(httpResponse.statusCode);
    if (httpResponse.statusCode == 200 || httpResponse.statusCode == 201) {
      print('------------------------------------');
      var jsonData = json.decode(httpResponse.body);
      Order order = Order.fromJson(jsonData);

      print('------------Order details------------------------');
      final orderId = order.id;

      for (final cartItem in _cartController.dishets) {
        final response = await httpGateway.post(
          "http://192.168.100.214:8084/smartmssa/order-management/api/order-details",
          json.encode({
            'quantity': cartItem.quantity,
            'disheId': cartItem.id,
            "orders": {
              "id": orderId,
            }, // Assuming name is the dish ID
          }),
        );
        print(response.statusCode);
      }

      log(order.toString());
      return order;
    }
  }

  // setDishetToOrder(idorder, idDishet, quantite) async {
  //   final httpResponse = await httpGateway.post(
  //     "http://192.168.100.214:8084/smartmssa/order-management/api/order-details",

  //   )
  // }
}
