import 'dart:convert';
import 'dart:developer';

import 'package:delivery_user_app/app_config/http_gateway.dart';
import 'package:delivery_user_app/features/order/models/restaurant.dart';
import 'package:delivery_user_app/shared/service/base_url.dart';

class HomepageServices {
  HttpGateway httpGateway = HttpGateway();
  getRestaurants() async {
    final httpResponse = await httpGateway.get("${BaseUrls.apiUrl}/restaurant");
    if (httpResponse.statusCode == 200) {
      print('------------------------------------');
      List<Restaurant> restaurants = (json.decode(httpResponse.body) as List)
          .map((data) => Restaurant.fromJson(data))
          .toList();
      log(restaurants.toString());
      return restaurants;
    }

   
    return [];
  }
}
