import 'dart:convert';
import 'dart:developer';

import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:delivery_user_app/features/order/models/menu.dart';
import 'package:delivery_user_app/shared/service/base_url.dart';

import '../../../app_config/http_gateway.dart';

class RestaurantDetailServices {
  Future<List<Menu>> getMenus(restaurantId) async {
    final httpResponse = await httpGateway
        .get("${BaseUrls.apiUrl}/menus/restaurant/$restaurantId");
    if (httpResponse.statusCode == 200) {
      log(httpResponse.body);
      print('------------------------------------');
      List<Menu> menus = (json.decode(httpResponse.body) as List)
          .map((data) => Menu.fromJson(data))
          .toList();
      log(menus.toString());
      return menus;
    }
    return [];
  }

  HttpGateway httpGateway = HttpGateway();
  Future<List<FastFoodModel>> getPopularFoods(restaurantId) async {
    final httpResponse = await httpGateway
        .get("${BaseUrls.apiUrl}/menus/restaurant/$restaurantId");
    if (httpResponse.statusCode == 200) {
      log(httpResponse.body);
      print('------------------------------------');
      List<FastFoodModel> foods = (json.decode(httpResponse.body) as List)
          .map((data) => FastFoodModel.fromJson(data))
          .toList();
      log(foods.toString());
      return foods;
    }

    return [
      FastFoodModel(
          image: "assets/images/food.jpg",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300"),
      FastFoodModel(
          image: "assets/images/food.jpg",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300"),
      FastFoodModel(
          image: "assets/images/food.jpg",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300"),
      FastFoodModel(
          image: "assets/images/food.jpg",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300"),
      FastFoodModel(
          image: "assets/images/food.jpg",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300"),
    ];
  }

  Future<List<FastFoodModel>> getDishesByMenu(String menuId) async {
    final httpResponse =
        await httpGateway.get("${BaseUrls.apiUrl}/dishes/menus/$menuId");
    if (httpResponse.statusCode == 200) {
      log(httpResponse.body);
      print('------------------------------------');
      List<FastFoodModel> foods = (json.decode(httpResponse.body) as List)
          .map((data) => FastFoodModel.fromJson(data))
          .toList();
      log(foods.toString());
      return foods;
    }
    return [];
  }

  List<FastFoodModel> getPizzaFoods() {
    //add logique integration api
    return [
      FastFoodModel(
          image: "assets/images/margerita.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
      FastFoodModel(
          image: "assets/images/margerita.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
      FastFoodModel(
          image: "assets/images/margerita.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
      FastFoodModel(
          image: "assets/images/margerita.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
      FastFoodModel(
          image: "assets/images/margerita.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
    ];
  }

  List<FastFoodModel> getDrink() {
    return [
      FastFoodModel(
          image: "assets/images/boison.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
      FastFoodModel(
          image: "assets/images/boison.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
      FastFoodModel(
          image: "assets/images/boison.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
      FastFoodModel(
          image: "assets/images/boison.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
      FastFoodModel(
          image: "assets/images/boison.png",
          name: "Spagheti polongese",
          rating: 3,
          discount: "10",
          price: "300",
          description: 'Moyen | Fromage, oignon et purée de tomates'),
    ];
  }
}
