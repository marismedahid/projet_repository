import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:get/get.dart';

class CartController extends GetxController {
  var dishets = <FastFoodModel>[].obs;

  void addToCart(FastFoodModel item) {
    dishets.add(item);
    update();
  }

  double calculateTotalPrice() {
    double totalPrice = 0.0;
    for (var item in dishets) {
      try {
        final double parsedPrice = double.parse(item.price ?? '0.0');
        final int parsedQuantity = int.parse(item.quantity ?? '1');
        totalPrice += parsedPrice * parsedQuantity;
      } catch (e) {
        // Handle parsing errors (e.g., invalid price format)
        print('Error parsing price or quantity: $e');
      }
    }
    return totalPrice;
  }
}

class Item {
  final String name;
  final double price;

  Item({required this.name, required this.price});
}
