import 'package:delivery_user_app/features/order/models/menu.dart';
import 'package:delivery_user_app/features/order/services/restaurant_detail_services.dart';
import 'package:delivery_user_app/features/order/widgets/drink.dart';
import 'package:delivery_user_app/features/order/widgets/fast_food_widget.dart';
import 'package:delivery_user_app/features/order/widgets/piza.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class DetailsController extends GetxController {
  final RestaurantDetailServices _restaurantDetailServices =
      RestaurantDetailServices();
  var isLoading = false.obs;
  var currentIndex = 0.obs;

  List<Menu> menus = [];

  setCurrentIndex(int newValue) {
    currentIndex.value = newValue;
  }

  loadMenus(restaurantId) async {
    setIsLoading(true);
    menus = await _restaurantDetailServices.getMenus(restaurantId);
    
    setIsLoading(false);
  }

  setIsLoading(bool newValue) {
    isLoading.value = newValue;
  }

  final types = ["Popular", "Pizza", "Drink", "Fast food", "Burger"];
  final contents = [
    const SizedBox(),
    const SizedBox(),
    Drink(),
    Text("Burger"),
    Text("Fast food"),
  ];

  getCurrentContent(
    index,
    restaurantId,
  ) {
    return PopularFoods(restaurantId: restaurantId);
  }

  getContent(
    String menuId,
  ) {
    return Pizza(menuId: menuId);
  }
}
