import 'package:delivery_user_app/features/order/models/fastfood_model.dart';
import 'package:delivery_user_app/features/order/services/restaurant_detail_services.dart';
import 'package:get/get.dart';

class RestaurantDetailController extends GetxController {
  final RestaurantDetailServices _restaurantDetailServices =
      RestaurantDetailServices();
  List<FastFoodModel> popularFoods = [];
  List<FastFoodModel> pizzaFoods = [];
  List<FastFoodModel> drinks = [];
  List<FastFoodModel> foods = [];

  var isLoading = false.obs;

  setIsLoading(bool newValue) {
    isLoading.value = newValue;
  }

  loadPopularFoods(restaurantId) async {
    setIsLoading(true);
    popularFoods =
        await _restaurantDetailServices.getPopularFoods(restaurantId);
    setIsLoading(false);
  }

  loadPizzaFoods() {
    setIsLoading(true);
    print(_restaurantDetailServices.getPizzaFoods());
    print("-----------------------------------------");
    pizzaFoods = _restaurantDetailServices.getPizzaFoods();
    setIsLoading(false);
  }

  loadDrinkFoods() {
    setIsLoading(true);
    print("------------drink------------------------");
    print(_restaurantDetailServices.getDrink());
    print("-----------------------------------------");
    drinks = _restaurantDetailServices.getDrink();

    setIsLoading(false);
  }

  loadFoods(menuId) async {
    setIsLoading(true);
    foods = await _restaurantDetailServices.getDishesByMenu(menuId);
    print(foods);
    setIsLoading(false);
  }
}
