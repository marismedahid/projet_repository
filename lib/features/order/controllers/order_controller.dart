import 'package:delivery_user_app/features/order/services/orders_services.dart';
import 'package:get/get.dart';

class OrderController extends GetxController {
  OrderServices _orderServices = OrderServices();
  var isLoading = false.obs;

  createOrder() {
    setIsLoading(true);
    _orderServices.createOrder();
    setIsLoading(false);
  }

  setIsLoading(bool newValue) {
    isLoading.value = newValue;
  }
}
