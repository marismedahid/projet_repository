import 'package:delivery_user_app/features/order/widgets/info_suivi_commande.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'dart:async';

class TrakingController extends GetxController {
// Define as RxList
  // Declare your variables
  var cameraPosition = const CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 10,
  ).obs;

  // Method to update camera position
  void updateCameraPosition(CameraPosition newPosition) {
    cameraPosition.value = newPosition;
  }

  PolylinePoints polylinePoints = PolylinePoints();
  RxList<LatLng> polylineCoordinates = <LatLng>[].obs;
  Future drawRoute(LatLng source, LatLng destination) async {
    polylineCoordinates.clear();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      'AIzaSyCKmTCvOcyjC-5uGe3Ml-zluIqBivkya8o', // Replace with your Google Maps API key
      PointLatLng(source.latitude, source.longitude),
      PointLatLng(destination.latitude, destination.longitude),
    );

    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
  }

  var selectedOption = "".obs; // Observable to store the selected option

  // Function to handle option selection
  void selectOption(String option) {
    selectedOption.value = option;
    // You can add additional logic here if needed
  }

  void showMyBottomSheet() {
    Get.bottomSheet(const InfoSuiviCommande(
      code: '0xff',
      source: 'Palacio',
      destination: '123 rue moktar dadah, tvz',
      namedriver: 'Sidi',
      telephonedriver: '345678',
    ));
  }

  RxInt remainingTime = 35.obs;
  RxInt time = 35.obs;
  void startCountdown() {
    // Decrement remainingTime every second until it reaches 0
    Timer.periodic(const Duration(seconds: 60), (timer) {
      if (remainingTime.value > 0) {
        remainingTime.value--;
        update();
      } else {
        timer.cancel(); // Stop the timer when time is up
      }
    });
  }
}
