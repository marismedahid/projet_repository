import 'package:delivery_user_app/features/order/models/restaurant.dart';
import 'package:delivery_user_app/features/order/services/homepage_services.dart';
import 'package:get/get.dart';

class HomepageController extends GetxController {
  final HomepageServices _homepageServices = HomepageServices();
  var isLoadingRestaurants = false.obs;
  List<Restaurant> restaurants = [];

  loadRestaurants() async {
    setIsLoadingHotels(true);
    restaurants =  await _homepageServices.getRestaurants();
    setIsLoadingHotels(false);
  }

  setIsLoadingHotels(bool newValue) {
    isLoadingRestaurants.value = newValue;
  }
  HomepageController(){
    loadRestaurants();
  }
}
