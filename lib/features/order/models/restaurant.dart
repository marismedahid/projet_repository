import 'dart:math';

class Restaurant {
  const Restaurant({
    required this.id, 
    required this.image,
    this.discount,
    required this.isFavorite,
    required this.logo,
    required this.name,
    required this.rating,
    required this.reviewers,
    this.minimumToDeliver = "0",
    required this.minTimeToDeliver,
    required this.maxTimeToDeliver,
    required this.deliveryPrice,
  });

  final String image;
  final String? discount;
  final bool isFavorite;
  final String logo;
  final String name;
  final String rating;
  final String reviewers;
  final String? minimumToDeliver;
  final String minTimeToDeliver;
  final String maxTimeToDeliver;
  final String deliveryPrice;
  final int id;

  factory Restaurant.fromJson(Map<String, dynamic> json) {
    return Restaurant(
      id: json["id"],
      // image: json['urlImage'] != null
      //     ? json['urlImage'] as String
      //     : _generateRandomImage(),
      image: _generateRandomImage(),
      discount: json['discount'] as String?,
      isFavorite: json.containsKey('isFavorite')
          ? json['isFavorite'] as bool
          : _generateRandomIsFavorite(),
      // logo:
      //     json['logo'] != null ? json['logo'] as String : _generateRandomLogo(),
      logo: _generateRandomLogo(),
      name:
          json['name'] != null ? json['name'] as String : _generateRandomName(),
      rating: json['rating'] != null
          ? json['rating'] as String
          : _generateRandomRating(),
      reviewers: json['reviewers'] != null
          ? json['reviewers'] as String
          : _generateRandomReviewers(),
      minimumToDeliver: json['minimumToDeliver'] != null
          ? json['minimumToDeliver'] as String
          : _generateRandomMinimumToDeliver(),
      minTimeToDeliver: json['minTimeToDeliver'] != null
          ? json['minTimeToDeliver'] as String
          : _generateRandomMinTimeToDeliver(),
      maxTimeToDeliver: json['maxTimeToDeliver'] != null
          ? json['maxTimeToDeliver'] as String
          : _generateRandomMaxTimeToDeliver(),
      deliveryPrice: json['deliveryPrice'] != null
          ? json['deliveryPrice'] as String
          : _generateRandomDeliveryPrice(),
    );
  }

  static String _generateRandomImage() {
    // Provide a default image when the urlImage is null
    return "assets/images/image_02.png";
  }

  static String _generateRandomLogo() {
    // Provide a default logo when the logo is null
    return "assets/images/logo_01.png";
  }

  static String _generateRandomName() {
    // Provide a default name
    return "Default Restaurant Name";
  }

  static String _generateRandomRating() {
    // Generate a random rating between 0 and 5 with one decimal place
    final double randomRating = Random().nextDouble() * 5;
    return randomRating.toStringAsFixed(1);
  }

  static String _generateRandomReviewers() {
    // Generate a random number of reviewers between 0 and 1000
    final int randomReviewers = Random().nextInt(1001);
    return randomReviewers.toString();
  }

  static String _generateRandomMinimumToDeliver() {
    // Provide a default minimum to deliver
    return "30";
  }

  static String _generateRandomMinTimeToDeliver() {
    // Provide a default minimum time to deliver
    return "20";
  }

  static String _generateRandomMaxTimeToDeliver() {
    // Provide a default maximum time to deliver
    return "45";
  }

  static String _generateRandomDeliveryPrice() {
    // Provide a default delivery price
    return "5.99";
  }

  static bool _generateRandomIsFavorite() {
    // Generate a random boolean value
    return Random().nextBool();
  }

  Map<String, dynamic> toJson() {
    return {
      'image': image,
      'discount': discount,
      'isFavorite': isFavorite,
      'logo': logo,
      'name': name,
      'rating': rating,
      'reviewers': reviewers,
      'minimumToDeliver': minimumToDeliver,
      'minTimeToDeliver': minTimeToDeliver,
      'maxTimeToDeliver': maxTimeToDeliver,
      'deliveryPrice': deliveryPrice,
    };
  }
}
