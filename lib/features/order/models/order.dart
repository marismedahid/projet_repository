class Order {
  int? id;
  String? code;
  DateTime? startDate;
  DateTime? endDate;
  String? lat;
  String? lng;
  String? shippingAddress;
  String? userId;
  OrderStatus? orderStatus;
  DateTime? updateStatusDate;

  Order({
    this.id,
    this.code,
    this.startDate,
    this.endDate,
    this.lat,
    this.lng,
    this.shippingAddress,
    this.userId,
    this.orderStatus,
    this.updateStatusDate,
  });

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
      id: json['id'] as int,
      code: json['code'] as String,
      startDate: DateTime.parse(json['startDate']),
      endDate: DateTime.parse(json['endDate']),
      lat: json['lat'] as String,
      lng: json['lng'] as String,
      shippingAddress: json['shippingAddress'] as String,
      userId: json['userId'] as String,
      orderStatus: OrderStatus.fromJson(json['orderStatus']),
      updateStatusDate: DateTime.parse(json['updateStatusDate']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code?? "null",
      'startDate': "2024-05-12T19:08:02.082Z",
      'endDate': "2024-05-12T19:08:02.082Z",
      'lat': lat?? "12",
      'lng': lng?? "13",
      'shippingAddress': shippingAddress ?? "00.000.000.0.0",
      'userId': userId ?? "",
      'orderStatus': orderStatus!= null? orderStatus!.toJson() : {"id" : 2, "name" : "null"},
    };
  }
}

class OrderStatus {
  int id;
  String name;

  OrderStatus({
    required this.id,
    required this.name,
  });

  factory OrderStatus.fromJson(Map<String, dynamic> json) {
    return OrderStatus(
      id: json['id'] as int,
      name: json['name'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
    };
  }
}
