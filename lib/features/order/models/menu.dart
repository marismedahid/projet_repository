class Menu {
  Menu({this.id, this.name});

  String? id;
  String? name;

  factory Menu.fromJson(Map<String, dynamic> json) {
    return Menu(
      id: json['id'].toString() as String?,
      name: json['name'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (id != null) {
      data['id'] = id;
    }
    if (name != null) {
      data['name'] = name;
    }
    return data;
  }
}
