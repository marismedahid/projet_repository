import 'dart:math';

class FastFoodModel {
  FastFoodModel(
      {this.id,
      this.image,
      this.name,
      this.rating,
      this.discount,
      this.price,
      this.description,
      this.quantity,
      this.note});
  int? id;
  final String? image;
  final String? name;
  final int? rating;
  final String? discount;
  final String? price;
  final String? description;
  String? quantity;
  final String? note;

  factory FastFoodModel.fromJson(Map<String, dynamic> json) {
    return FastFoodModel(
        id: json['id'] as int,
        image: json['image'] != null
            ? json['image'] as String
            : _generateRandomImage(),
        name: json['name'] != null
            ? json['name'] as String
            : _generateRandomName(),
        rating: json['rating'] != null
            ? json['rating'] as int
            : _generateRandomRating(),
        discount: json['discount'] as String?,
        price: json['price'] != null
            ? json['price'].toString() as String?
            : _generateRandomPrice(),
        description: json['description'] != null
            ? json['description'].toString() as String?
            : _generateRandomDescription(),
        quantity: json['quantityt'],
        note: json['note'] != null
            ? json['note'].toString() as String?
            : _generateRandomNote());
  }

  static String _generateRandomImage() {
    // Provide a default image when the image is null
    return "assets/images/image_02.png";
  }

  static String _generateRandomName() {
    // Provide a default name when the name is null
    return "Default Fast Food Name";
  }

  static int _generateRandomRating() {
    // Generate a random rating between 1 and 5
    return Random().nextInt(5) + 1;
  }

  static String _generateRandomDiscount() {
    // Provide a default discount when the discount is null
    return "0%";
  }

  static String _generateRandomPrice() {
    // Provide a default price when the price is null
    return "10.00";
  }

  static String _generateRandomDescription() {
    // Provide a default price when the price is null
    return 'No description available';
  }

  // static String _generateRandomQuantite() {
  //   // Provide a default price when the price is null
  //   return '1.0';
  // }

  static String _generateRandomNote() {
    // Provide a default price when the price is null
    return 'No note available';
  }

  Map<String, dynamic> toJson() {
    return {
      'image': image,
      'name': name,
      'rating': rating,
      'discount': discount,
      'price': price,
      'quantity': quantity
    };
  }
}
