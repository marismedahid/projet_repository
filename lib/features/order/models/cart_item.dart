class CartItem {
  CartItem({
    this.id,
    this.imageUrl,
    this.name,
    this.note,
    this.price,
    this.quantity,
  });
  int? id;
  final String? imageUrl;
  final String? name;
  final String? note;
  final String? price;
  final String? quantity;

  factory CartItem.fromJson(Map<String, dynamic> json) {
    return CartItem(
      id: json['id'] as int,
      imageUrl: json['imageUrl'] as String,
      name: json['name'] as String,
      note: json['note'] as String,
      price: json['price'] as String,
      quantity: json['quantity'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'imageUrl': imageUrl,
      'name': name,
      'note': note,
      'price': price,
      'quantity': quantity,
    };
  }
}
