import 'package:delivery_user_app/features/order/screens/cart_screen.dart';
import 'package:delivery_user_app/features/order/screens/details_screen.dart';
import 'package:delivery_user_app/features/order/screens/homepage.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

class OrderManagementRoutes {
  static const String homepage = '/homepage';
  static const String cartScreen = '/cart-screen';
  static const String restaurantDetails = '/restaurant-details';

  static final List<GetPage> routes = [
    GetPage(name: homepage, page: () => Homepage()),
    GetPage(name: cartScreen, page: () => CartScreen()),
    GetPage(name: restaurantDetails, page: () => DetailsScreen()),
  ];
}
