import 'package:delivery_user_app/features/order/app_routes.dart';
import 'package:delivery_user_app/features/order/screens/details_screen.dart';
import 'package:delivery_user_app/features/user_management/app_routes.dart';
import 'package:delivery_user_app/shared/screens/navigation_page.dart';
import 'package:delivery_user_app/shared/splash_screen.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

class AppRoutes {
  static const String splash = '/splash';
  static const String home = '/home';
  static const String navigationPage = '/navigationpage';
  static final List<GetPage> routes = [
    GetPage(
      name: navigationPage,
      page: () => NavigationPage(),
    ),
    GetPage(
      name: splash,
      page: () => SplashScreen(),
    ),
    GetPage(
      name: home,
      page: () => DetailsScreen(),
    ),
    ...UserManagementRoutes.routes,
    ...OrderManagementRoutes.routes
  ];
}
